﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Android.Content;
using Android.Gms.Location;
using Android.OS;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PETni.Business;
using Plugin.Connectivity;
using Plugin.Geolocator;
using Quobject.SocketIoClientDotNet.Client;
using Xamarin.Forms;

namespace PETni.Droid
{
    //[BroadcastReceiver]
    [BroadcastReceiver(Enabled = true)]
    public class ToastBroadcast : BroadcastReceiver
    {
        private static Socket socket;
        public bool IsUnsubscirbedFromMessage = true;

        public async override void OnReceive(Context context, Intent intent)
        {
            try
            {
                await InitSocket(context);
                //Toast.MakeText(context, string.Format("Starting TIME IS {0}", DateTime.Now.ToString("hh:mm:ss")), ToastLength.Long).Show();
                //Vibrator vibrator = (Vibrator)context.GetSystemService(Context.VibratorService);
                //vibrator.Vibrate(500);
                var connectivity = CrossConnectivity.Current;
                if (connectivity.IsConnected)
                {
                    //}

                    //if (NetworkProvider.IsAvailable())
                    //{
                    Toast.MakeText(context, string.Format("THE TIME IS {0}", DateTime.Now.ToString("hh:mm:ss")), ToastLength.Long).Show();
                    //Vibrator vibrator = (Vibrator)context.GetSystemService(Context.VibratorService);
                    //vibrator.Vibrate(500);

                    UserPreference userPreference = new UserPreference();
                    #region temp
                    HttpBaseClient httpBaseClient = new HttpBaseClient("https://test.fr167.com/api/ecommerce/products-by-category");
                    var responseCRServiceContent = await httpBaseClient.Get();
                    Toast.MakeText(context, responseCRServiceContent, ToastLength.Long).Show();

                    #endregion
                    var ServiceList = new List<BackServiceDetails>();
                    var ServiceStr = userPreference.GetString(Enums.KeyBox.ServicesData);
                    if (ServiceStr != null)
                        ServiceList = userPreference.Deserialize<List<BackServiceDetails>>(ServiceStr);

                    if (ServiceList != null && ServiceList.Count > 0)
                    {
                        List<UpdateLocationRequest> LocationList = new List<UpdateLocationRequest>();
                        var LocationStr = userPreference.GetString(Enums.KeyBox.LocationData);

                        if (LocationStr != null)
                            LocationList = userPreference.Deserialize<List<UpdateLocationRequest>>(LocationStr);

                        if (LocationList == null)
                            LocationList = new List<UpdateLocationRequest>();

                        UpdateLocationRequest req = new UpdateLocationRequest();

                        var locator = CrossGeolocator.Current;
                        locator.DesiredAccuracy = 50;

                        double Latitude = 0, Longitude = 0;

                        if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                        {
                            var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

                            Latitude = position.Latitude;
                            Longitude = position.Longitude;
                        }
                        else
                        {
                            //ProgressDialog.HideProgress();
                            //Utilities.ShowOkAlert(Constants.NetworkAlert);
                            return;
                        }

                        foreach (var item in ServiceList)
                        {
                            req.id = item.id;
                            req.request_id = item.request_id;
                            req.latitude = Latitude;
                            req.longitude = Longitude;
                            req.current_date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            var data = JsonConvert.SerializeObject(req);

                            //LocationList.Add(req);
                            MessagingCenter.Send<UpdateLocationRequest, string>(req, "UpdateLocation", data);

                            //Toast.MakeText(context, string.Format("Latitude : {0}, Longitude : {1}, Id : {2} Req_Id : {3}", Latitude, Longitude, item.id, item.request_id), ToastLength.Long).Show();
                            //vibrator.Vibrate(500);
                        }

                        userPreference.SetString(Enums.KeyBox.LocationData, userPreference.Serialize(LocationList));
                        var exc = userPreference.GetString(Enums.KeyBox.Exception);

                    }
                }
                //Toast.MakeText(context, string.Format("Exception pela TIME IS {0}", DateTime.Now.ToString("hh:mm:ss")), ToastLength.Long).Show();
            }
            catch (Exception ex)
            {
                UserPreference userPreference = new UserPreference();
                userPreference.SetString(Enums.KeyBox.TempData, ex.Message);
                userPreference.SetString(Enums.KeyBox.TempData, ex.Data.ToString());
                Toast.MakeText(context, string.Format("Error : {0}", ex.Message + " " + ex.Data), ToastLength.Long).Show();
                //Vibrator vibrator = (Vibrator)context.GetSystemService(Context.VibratorService);
                //vibrator.Vibrate(500);
                Logger.SendErrorLog(ex);
            }
            finally
            {
                //Toast.MakeText(context, string.Format("finally ma avyu "), ToastLength.Long).Show();
                //Vibrator vibrator = (Vibrator)context.GetSystemService(Context.VibratorService);
                //vibrator.Vibrate(500);
            }
        }

        #region Socket
        public async Task InitSocket(Context context)
        {
            try
            {
                if (socket != null)
                {
                    socket.Close();
                }


                string url = "http://172.81.117.236:3000";
                socket = IO.Socket(url);
                ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
                socket.On("connect", data =>
                {
                    //Toast.MakeText(context, string.Format("Socket connect ma avyu"), ToastLength.Long).Show();

                    if (data != null)
                    {
                        var json = data as JObject;
                        string j = json.ToString(Newtonsoft.Json.Formatting.None);
                        // Data d = JsonConvert.DeserializeObject<Data>(j);
                        //ChatMessageRestService.cmrr = d.data;
                        try
                        {

                        }
                        catch (Exception ex)
                        {
                            //System.Diagnostics.Debug.WriteLine(ex);
                            Logger.SendErrorLog(ex);
                        }
                        finally
                        {
                            //ProgressDialog.HideProgress();
                        }
                    }
                });

                socket.Connect();
                socket.On(Socket.EVENT_ERROR, (obj) =>
                {
                    //Toast.MakeText(context, string.Format("Socket Event_Error => " + obj.ToString()), ToastLength.Long).Show();
                });
                socket.On(Socket.EVENT_CONNECT_ERROR, (obj) =>
                {
                    //Toast.MakeText(context, string.Format("Socket EVENT_CONNECT_ERROR => " + obj.ToString()), ToastLength.Long).Show();

                });
                socket.On(Socket.EVENT_RECONNECT_ERROR, (obj) =>
                {
                    //Toast.MakeText(context, string.Format("Socket EVENT_RECONNECT_ERROR => " + obj.ToString()), ToastLength.Long).Show();

                });
                socket.On(Socket.EVENT_CONNECT_TIMEOUT, (obj) =>
                {
                    //Toast.MakeText(context, string.Format("Socket EVENT_CONNECT_TIMEOUT => " + obj.ToString()), ToastLength.Long).Show();

                });
                socket.On(Socket.EVENT_RECONNECT_FAILED, (obj) =>
                {
                    //Toast.MakeText(context, string.Format("Socket EVENT_RECONNECT_FAILED => " + obj.ToString()), ToastLength.Long).Show();

                });
                socket.On(Socket.EVENT_CONNECT, (obj) =>
                {
                    Vibrator vibrator = (Vibrator)context.GetSystemService(Context.VibratorService);
                    vibrator.Vibrate(500);
                    //Toast.MakeText(context, string.Format("Socket EVENT_CONNECT => " + obj.ToString()), ToastLength.Long).Show();

                });

                if (IsUnsubscirbedFromMessage)
                {
                    SubscribeForMessages(context);
                }
            }
            catch (Exception ex)
            {
                //Toast.MakeText(context, string.Format("Error Socket Init : {0}", ex.Message + " " + ex.Data), ToastLength.Long).Show();

                Logger.SendErrorLog(ex);
            }


        }

        public void SubscribeForMessages(Context context)
        {
            try
            {
                MessagingCenter.Subscribe<UpdateLocationRequest, string>(this, "UpdateLocation", (sender, arg) =>
                {

                    var data = JsonConvert.DeserializeObject<UpdateLocationRequest>(arg);

                    socket.Emit("location", arg);
                    //Vibrator vibrator = (Vibrator)context.GetSystemService(Context.VibratorService);
                    //vibrator.Vibrate(500);
                    //Toast.MakeText(context, string.Format("Data send Success"), ToastLength.Long).Show();
                    IsUnsubscirbedFromMessage = false;
                });
            }
            catch (Exception ex)
            {
                UserPreference userPreference = new UserPreference();
                userPreference.SetString(Enums.KeyBox.Exception, ex.Message);
                //Toast.MakeText(context, string.Format("Error SubscribeForMessages : {0}", ex.Message + " " + ex.Data), ToastLength.Long).Show();
                //System.Diagnostics.Debug.WriteLine(ex);
                Logger.SendErrorLog(ex);
            }
        }
        #endregion
    }
}