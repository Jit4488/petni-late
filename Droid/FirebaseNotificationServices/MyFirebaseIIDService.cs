﻿using System;
using Android.App;
using Firebase.Iid;
using PETni.Business;
using Xamarin.Forms;

namespace PETni.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";

        public override void OnTokenRefresh()
        {
            try
            {
                var refreshedToken = FirebaseInstanceId.Instance.Token;
                Android.Util.Log.Debug(TAG, "Refreshed token: " + refreshedToken);
                DependencyService.Get<IUserPreferences>().SetString(Enums.KeyBox.Token, refreshedToken);
                SendRegistrationToServer(refreshedToken);
                base.OnTokenRefresh();
            }
            catch (Exception ex)
            {

            }
        }
        //store app tocken to local
        void SendRegistrationToServer(string token)
        {
        }
    }
}