﻿using System;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Firebase.Messaging;
using Newtonsoft.Json;
using PETni.Business;
using Xamarin.Forms;
using static PETni.Business.Enums;

namespace PETni.Droid
{
    [Service]
    [IntentFilter(new string[] { "com.google.firebase.MESSAGING_EVENT" })]
    [IntentFilter(new string[] { Intent.ActionBootCompleted })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        private static int i = 0;
        const string TAG = "MyFirebaseMsgService";

        public override void OnMessageReceived(RemoteMessage message)
        {
            try
            {
                var id = DependencyService.Get<IUserPreferences>().GetString(Enums.KeyBox.UserId);
                if (id != null && !string.IsNullOrEmpty(id))
                {
                    string strNotification = string.Empty;

                    if (message.Data.TryGetValue("data", out strNotification))
                    {
                        SendNotification(Regex.Unescape(strNotification), Convert.ToString(message.Data["NotificationType"]), message.GetNotification().Body, message.GetNotification().Title);
                    }
                    else
                    {
                        SendNotification(string.Empty, string.Empty, message.GetNotification().Body, message.GetNotification().Title);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public override void HandleIntent(Intent intent)
        {
            try
            {
                if (intent.Extras != null)
                {
                    var builder = new RemoteMessage.Builder("MyFirebaseMessagingService");

                    foreach (string key in intent.Extras.KeySet())
                    {
                        builder.AddData(key, intent.Extras.Get(key).ToString());
                    }

                    this.OnMessageReceived(builder.Build());
                }
                else
                {
                    base.HandleIntent(intent);
                }
            }
            catch (Exception)
            {
                base.HandleIntent(intent);
            }
        }

        public override void OnSendError(string msgId, Java.Lang.Exception exception)
        {
            base.OnSendError(msgId, exception);
        }



        void SendNotification(string messageBody, string notificationType, string notificationBodyDisplay, string notificationTitleDisplay)
        {
            try
            {
                string messageTitle = notificationType;
                //string messageTitle = Constants.APP_NAME;

                //if (notificationType == NotificationType.task.ToString())
                //{
                //    var Task = JsonConvert.DeserializeObject<Tasks>(messageBody);
                //    messageBody = Task.title;
                //    AppSession.Instance.Task = Task;
                //    MessagingCenter.Send<MyFirebaseMessagingService, Tasks>(this, NotificationType.task.ToString(), Task);
                //}
                //else if (notificationType == NotificationType.chat.ToString())
                //{
                //    var ChatMessageBody = JsonConvert.DeserializeObject<ChatNotification>(messageBody);
                //    if (ChatPage._taskId != null && ChatPage._taskId == ChatMessageBody.taskId)
                //        return;
                //    messageBody = ChatMessageBody.ChatName + " : " + ChatMessageBody.message;
                //    AppSession.Instance.chat = ChatMessageBody;
                //    MessagingCenter.Send<MyFirebaseMessagingService, ChatNotification>(this, NotificationType.chat.ToString(), ChatMessageBody);
                //}
                //else if (notificationType == NotificationType.Group.ToString())
                //{
                //    var Group = JsonConvert.DeserializeObject<GroupData>(messageBody);
                //    messageBody = Group.name;
                //    MessagingCenter.Send<MyFirebaseMessagingService, GroupData>(this, NotificationType.Group.ToString(), Group);
                //}
                //else
                //{
                //    messageBody = TaskOrganiser.Business.Constants.APP_NAME;
                //}

                NotificationManager notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
                String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

                if (Build.VERSION.SdkInt >= Build.VERSION_CODES.O)
                {
                    NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.ImportanceMax);

                    notificationChannel.EnableLights(true);
                    notificationChannel.ShouldVibrate();
                    notificationChannel.EnableVibration(true);
                    notificationManager.CreateNotificationChannel(notificationChannel);
                }

                var intent = new Intent(this, typeof(MainActivity));
                intent.AddFlags(ActivityFlags.ClearTop);
                var pendingIntent = PendingIntent.GetActivity(this, 0 /* Request code */, intent, PendingIntentFlags.OneShot);

                var defaultSoundUri = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
                var notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .SetSmallIcon(Resource.Drawable.ic_launcher)
                    .SetContentTitle(notificationTitleDisplay)//pnr.NotificationType.ToString())
                    .SetContentText(notificationBodyDisplay)
                    .SetAutoCancel(true)
                    .SetSound(defaultSoundUri)
                    .SetContentIntent(pendingIntent);

                //var notificationManager = NotificationManager.FromContext(this);
                i++;
                notificationManager.Notify(i /* ID of notification */, notificationBuilder.Build());

            }
            catch (Exception ex)
            {
            }
        }

    }
}