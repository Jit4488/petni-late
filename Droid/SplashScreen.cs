﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Widget;

namespace PETni.Droid
{
    //@style/Theme.AppCompat.Light.NoActionBar
    //ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    //[Activity(Label = "PETni",Theme ="@style/SplashTheme", MainLauncher = true, NoHistory = true)]
    public class SplashScreen : Activity//Android.Support.V7.App.AppCompatActivity
    {
        Felipecsl.GifImageViewLibrary.GifImageView gifImageView;

        protected override void OnCreate(Bundle bundle)
        {
            
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.SplashScreenGif);
            // Create your application here

            //gifImageView = FindViewById<Felipecsl.GifImageViewLibrary.GifImageView>(Resource.Id.gifImageView);

            try
            {
                TextView versionText = FindViewById<TextView>(Resource.Id.version_number);
                versionText.Text = "1.0.0";
                versionText.SetTextColor(Color.Red);

                ImageView image = FindViewById<ImageView>(Resource.Id.imageView1);
                image.SetImageResource(Resource.Drawable.petni);


                //gifImageView.StopAnimation();
                ////Stream imageGif = Assets.Open("loader.gif");
                ////var v = ((Xamarin.Forms.StreamImageSource)imageGif).Stream(default(CancellationToken));
                ////Byte[] imageBytes = StreamToByteArray(imageGif);
                //gifImageView.SetImageResource(Resource.Drawable.icon);//.SetBytes(imageBytes);
                //gifImageView.StartAnimation();
                //Task.Delay(30000).Wait();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }


            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }

        static byte[] GetBytesFromStreamAsync(Stream stream)
        {
            using (stream)
            {
                if (stream == null || (int)stream.Length == 0)
                    return null;

                var bytes = new byte[stream.Length];
                if (stream.Read(bytes, 0, (int)stream.Length) > 0)
                    return bytes;
            }



            return null;
        }
        public static byte[] StreamToByteArray(Stream stream)
        {
            if (stream is MemoryStream)
            {
                return ((MemoryStream)stream).ToArray();
            }
            return null;
        }
    }
}
