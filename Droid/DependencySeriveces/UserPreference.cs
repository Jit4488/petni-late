﻿using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.Graphics;
using Newtonsoft.Json;
using PETni.Business;
using PETni.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(UserPreference))]
namespace PETni.Droid
{
    public class UserPreference : IUserPreferences
    {

        public void SetString(Enums.KeyBox key, string value)
        {
            var prefs = Application.Context.GetSharedPreferences(Constants.APP_NAME, FileCreationMode.Private);
            var prefsEditor = prefs.Edit();
            prefsEditor.PutString(key.ToString(), value);
            prefsEditor.Commit();
        }

        public string GetString(Enums.KeyBox key)
        {
            var prefs = Application.Context.GetSharedPreferences(Constants.APP_NAME, FileCreationMode.Private);
            return prefs.GetString(key.ToString(), null);
        }

        public byte[] ResizeImageAndroid(byte[] imageData, float width, float height)
        {
            // Load the bitmap 
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
            //
            float ZielHoehe = 0;
            float ZielBreite = 0;
            //
            var Hoehe = originalImage.Height;
            var Breite = originalImage.Width;
            //
            if (Hoehe > Breite) // Höhe (71 für Avatar) ist Master
            {
                ZielHoehe = height;
                float teiler = Hoehe / height;
                ZielBreite = Breite / teiler;
            }
            else // Breite (61 für Avatar) ist Master
            {
                ZielBreite = width;
                float teiler = Breite / width;
                ZielHoehe = Hoehe / teiler;
            }
            //
            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)ZielBreite, (int)ZielHoehe, false);
            // 
            using (System.IO.MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
                return ms.ToArray();
            }
        }


        public T Deserialize<T>(string payload)
        {
            T data = default(T);
            try
            {
                data = JsonConvert.DeserializeObject<T>(payload);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return data;
        }

        public string Serialize(object payload)
        {
            string data = null;
            try
            {
                data = JsonConvert.SerializeObject(payload);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return data;
        }
    }
}