﻿using System;
using System.IO;
using PETni.Droid;
using SQLite;
using Xamarin.Forms;
using PETni.Business;

[assembly: Dependency(typeof(SQLiteDataAccess))]
namespace PETni.Droid
{
    public class SQLiteDataAccess : ISQLiteHelper
    {
        public SQLiteDataAccess()
        {
        }

        public SQLiteConnection GetConnection()
        {
            var sqliteFilename = Constants.DatabaseName;
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, sqliteFilename);

            // This is where we copy in the prepopulated database
            if (!File.Exists(path))
            {
                File.Create(path);
            }
            var conn = new SQLite.SQLiteConnection(path);

            // Return the database connection 
            return conn;
        }
    }
}

