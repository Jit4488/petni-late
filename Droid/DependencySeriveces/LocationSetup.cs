﻿using System;
using System.Threading.Tasks;
using Android.Content;
using Android.Locations;
using PETni.Droid;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(LocationSetup))]
namespace PETni.Droid
{
    public class LocationSetup : ILocationSetup
    {
        public LocationSetup()
        {
        }

        public bool SetLocation()
        {
            LocationManager locationManager = (LocationManager)MainActivity.ShareActivityContext.GetSystemService(Context.LocationService);
            if (locationManager.IsProviderEnabled(LocationManager.GpsProvider) == false)
            {
                Intent gpsSettingIntent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                Forms.Context.StartActivity(gpsSettingIntent);
            }
            return true;
        }
    }
}
