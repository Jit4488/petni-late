﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Views.InputMethods;
using AndroidHUD;
using PETni.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ProgressDialogService))]
namespace PETni.Droid
{
    public class ProgressDialogService : IProgressDialog
    {
        public ProgressDialogService()
        {
        }
        public bool IsShowing { get; set; }

        #region IProgressDialog implementation

        public void Show()
        {
            this.IsShowing = true;
            ShowProgress(progress => AndHUD.Shared.Show(MainActivity.ShareActivityContext, null, progress, MaskType.Clear, TimeSpan.FromSeconds(100)));
        }

        public void Show(string message)
        {
            try
            {
                IsShowing = true;
                switch (message)
                {
                    case "Status Indicator Only":
                        AndHUD.Shared.Show(MainActivity.ShareActivityContext, null, -1, MaskType.Black, TimeSpan.FromSeconds(3));
                        break;
                    case "Status Indicator and Text":
                        AndHUD.Shared.Show(MainActivity.ShareActivityContext, "Loading...", -1, MaskType.Clear, TimeSpan.FromSeconds(3));
                        break;
                    case "Non-Modal Indicator and Text":
                        AndHUD.Shared.Show(MainActivity.ShareActivityContext, "Loading...", -1, MaskType.None, TimeSpan.FromSeconds(5));
                        break;
                    case "Progress Only":
                        ShowProgress(progress => AndHUD.Shared.Show(MainActivity.ShareActivityContext, null, progress, MaskType.Clear));
                        break;
                    case "Progress and Text":
                        ShowProgress(progress => AndHUD.Shared.Show(MainActivity.ShareActivityContext, "Loading... " + progress + "%", progress, MaskType.Clear));
                        break;
                    case "Success Image Only":
                        AndHUD.Shared.ShowSuccessWithStatus(MainActivity.ShareActivityContext, null, MaskType.Black, TimeSpan.FromSeconds(3));
                        break;
                    case "Success Image and Text":
                        AndHUD.Shared.ShowSuccessWithStatus(MainActivity.ShareActivityContext, "It Worked!", MaskType.Clear, TimeSpan.FromSeconds(3));
                        break;
                    case "Error Image Only":
                        AndHUD.Shared.ShowErrorWithStatus(MainActivity.ShareActivityContext, null, MaskType.Clear, TimeSpan.FromSeconds(3));
                        break;
                    case "Error Image and Text":
                        AndHUD.Shared.ShowErrorWithStatus(MainActivity.ShareActivityContext, "It no worked :(", MaskType.Black, TimeSpan.FromSeconds(3));
                        break;
                    case "Toast":
                        AndHUD.Shared.ShowToast(MainActivity.ShareActivityContext, "This is a toast... Cheers!", MaskType.Black, TimeSpan.FromSeconds(3), true);
                        break;
                    case "Toast Non-Centered":
                        AndHUD.Shared.ShowToast(MainActivity.ShareActivityContext, "This is a non-centered Toast...", MaskType.Clear, TimeSpan.FromSeconds(3), false);
                        break;
                    /*  case "Custom Image":
                        AndHUD.Shared.ShowImage(this, Resource.Drawable.ic_questionstatus, "Custom Image...", MaskType.Black, TimeSpan.FromSeconds(3));
                        break;*/
                    case "Click Callback":
                        AndHUD.Shared.ShowToast(MainActivity.ShareActivityContext, "Click this toast to close it!", MaskType.Clear, null, true, () => AndHUD.Shared.Dismiss(MainActivity.ShareActivityContext));
                        break;
                    case "Cancellable Callback":
                        AndHUD.Shared.ShowToast(MainActivity.ShareActivityContext, "Click back button to cancel/close it!", MaskType.None, null, true, null, () => AndHUD.Shared.Dismiss(MainActivity.ShareActivityContext));
                        break;
                    case "Long Message":
                        AndHUD.Shared.Show(MainActivity.ShareActivityContext, "This is a longer message to display!", -1, MaskType.Black, TimeSpan.FromSeconds(3));
                        break;
                    case "Really Long Message":
                        AndHUD.Shared.Show(MainActivity.ShareActivityContext, "This is a really really long message to display as a status indicator, so you should shorten it!", -1, MaskType.Black, TimeSpan.FromSeconds(3));
                        break;
                    case "Loading...":
                        AndHUD.Shared.Show(MainActivity.ShareActivityContext, "Loading...", -1, MaskType.Black, TimeSpan.FromSeconds(5));
                        break;
                    default:
                        AndHUD.Shared.Show(MainActivity.ShareActivityContext, message, -1, MaskType.Black, TimeSpan.FromSeconds(1));
                        break;
                }
            }
            catch (Exception ex)
            {
                ex.SendErrorLog();
            }
            //UIApplication.SharedApplication.InvokeOnMainThread (() => BTProgressHUD.Show (message, -1, ProgressHUD.MaskType.Gradient));
        }

        public void ShowToastMessage(string message)
        {
            AndHUD.Shared.ShowToast(MainActivity.ShareActivityContext, message, MaskType.Black, TimeSpan.FromSeconds(2), true);
        }

        public void ShowProgressWithMessage(string message, int completed, int Total)
        {
            var done = 100 * completed / Total;
            ShowProgress(progress => AndHUD.Shared.Show(MainActivity.ShareActivityContext, "Loading... " + done + "%", done, MaskType.Clear));
            //ShowProgressWithMessage(progress => AndHUD.Shared.Show(MainActivity.ShareActivityContext, message, progress, MaskType.Clear),done);
        }



        void ShowProgress(Action<int> action)
        {
            try
            {
                Task.Factory.StartNew(() =>
                {

                    //int progress = 0;

                    //while (progress <= 100)
                    //{
                    //  action(progress);

                    //  Thread.Sleep(500);
                    //  progress += 10;
                    //}

                    AndHUD.Shared.Dismiss(MainActivity.ShareActivityContext);
                });
            }
            catch (Exception ex)
            {
                ex.SendErrorLog();
            }
        }

        void ShowDemo(Action action)
        {
            Task.Factory.StartNew(() =>
            {

                //action();

                //Thread.Sleep(3000);

                AndHUD.Shared.Dismiss(MainActivity.ShareActivityContext);
            });
        }

        public void Dismiss()
        {
            IsShowing = false;
            AndHUD.Shared.Dismiss(MainActivity.ShareActivityContext);
        }

        public void CloseKeyBoard()
        {
            try
            {
                MainActivity.ShareActivityContext.Window.DecorView.ClearFocus();
                InputMethodManager inputMethodManager = (InputMethodManager)MainActivity.ShareActivityContext.GetSystemService(Activity.InputMethodService);
                inputMethodManager.HideSoftInputFromWindow(MainActivity.ShareActivityContext.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
            }
            catch (Exception ex)
            {
                ex.SendErrorLog();
            }
        }

        #endregion
    }
}
