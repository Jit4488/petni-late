﻿using System;
using Android.App;
using Android.Content;
using Android.Gms.Location;
using Android.OS;
using Android.Util;
using Android.Widget;
using AndroidHUD;
using PETni.Business;
using Plugin.LocalNotifications;

namespace PETni.Droid
{
    [Service(Exported = true)]
    public class LocationService : IntentService
    {
        static readonly string TAG = typeof(LocationService).FullName;
        static readonly int DELAY_BETWEEN_LOG_MESSAGES = 5000; // milliseconds
        static readonly int NOTIFICATION_ID = 10000;
        LocationRequest mLocationRequest;
        UtcTimestamper timestamper;
        bool isStarted;
        Handler handler;
        Action runnable;
        public static int count = 0;

        public LocationService()
        {
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        protected override void OnHandleIntent(Intent intent)
        {
            var notification = new Notification.Builder(this)
                               .SetSmallIcon(Android.Resource.Drawable.IcDialogInfo)
                               .SetContentTitle("Notification")
                               .SetContentText("Location Update")
                               .Build();
            ((NotificationManager)GetSystemService(NotificationService)).Notify((new Random()).Next(), notification);

        }

        public override void OnCreate()
        {
            try
            {
                base.OnCreate();
                Log.Info(TAG, "OnCreate: the service is initializing.");

                mLocationRequest = new LocationRequest();
                timestamper = new UtcTimestamper();
                handler = new Handler();

                // This Action is only for demonstration purposes.
                runnable = new Action(() =>
                {
                    if (timestamper != null)
                    {
                        Log.Debug(TAG, timestamper.GetFormattedTimestamp());
                        handler.PostDelayed(runnable, DELAY_BETWEEN_LOG_MESSAGES);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            try
            {
                //if (isStarted)
                //{
                //    Log.Info(TAG, "OnStartCommand: This service has already been started.");
                //}
                //else
                //{
                //Log.Info(TAG, "OnStartCommand: The service is starting.");

                //mLocationRequest.SetInterval(10000);
                //mLocationRequest.SetFastestInterval(10000 / 2);
                //mLocationRequest.SetPriority(LocationRequest.PriorityHighAccuracy);
                //mLocationRequest.SetMaxWaitTime(20000);

                //handler.PostDelayed(runnable, DELAY_BETWEEN_LOG_MESSAGES);

                using (var manager = (AlarmManager)GetSystemService(AlarmService))
                {
                    // Send a Notification in ~60 seconds and then every ~90 seconds after that....
                    var alarmIntent = new Intent(this, typeof(LocationService));
                    var pendingIntent = PendingIntent.GetService(this, 0, alarmIntent, PendingIntentFlags.CancelCurrent);
                    manager.SetInexactRepeating(AlarmType.RtcWakeup, 1000 * 5, 1000 * 10, pendingIntent);

                    CrossLocalNotifications.Current.Show(Constants.APP_NAME, "Location update" + count.ToString());
                    count++;
                }

                //    isStarted = true;
                //}
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            // This tells Android not to restart the service if it is killed to reclaim resources.
            return StartCommandResult.NotSticky;
        }

        public override void OnDestroy()
        {
            try
            {
                // We need to shut things down.
                Log.Debug(TAG, GetFormattedTimestamp());
                Log.Info(TAG, "OnDestroy: The started service is shutting down.");

                // Stop the handler.
                handler.RemoveCallbacks(runnable);

                // Remove the notification from the status bar.

                timestamper = null;
                isStarted = false;
                base.OnDestroy();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        /// <summary>
        /// This method will return a formatted timestamp to the client.
        /// </summary>
        /// <returns>A string that details what time the service started and how long it has been running.</returns>
        string GetFormattedTimestamp()
        {
            return timestamper?.GetFormattedTimestamp();
        }

        void DispatchNotificationThatServiceIsRunning()
        {

        }
    }
}
