﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content;
using PETni.Droid;

[assembly: ExportRenderer(typeof(Entry), typeof(BorderLessEntryRenderer))]
namespace PETni.Droid
{
    public class BorderLessEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
                Control.SetPadding(0, 5, 5, 5);
            }
        }
    }
}
