﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using PETni.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ViewCell), typeof(ExtendedViewCellRenderer))]
[assembly: ExportRenderer(typeof(ListView), typeof(ExtendedListViewRenderer))]
namespace PETni.Droid
{
    public class ExtendedViewCellRenderer : ViewCellRenderer
    {
        private Android.Views.View _cellCore;
        private Drawable _unselectedBackground;
        private Drawable _selectedBackground;
        private bool _selected;

        protected override Android.Views.View GetCellCore(Cell item,
            Android.Views.View convertView,
            ViewGroup parent,
            Context context)
        {
            _cellCore = base.GetCellCore(item, convertView, parent, context);

            _selected = false;
            _unselectedBackground = _cellCore.Background;
            return _cellCore;
        }

        protected override void OnCellPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            base.OnCellPropertyChanged(sender, args);

            if (args.PropertyName == "IsSelected")
            {
                _selected = !_selected;

                if (_selected)
                {
                    var extendedViewCell = sender as ViewCell;
                    _cellCore.SetBackgroundColor(Color.FromHex("#dae0e8").ToAndroid());
                }
                else
                {
                    _cellCore.SetBackground(_unselectedBackground);
                }
            }
        }
    }


    public class ExtendedListViewRenderer : ListViewRenderer
    {
        public ExtendedListViewRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                Control.SetSelector(Android.Resource.Color.Transparent);
                Control.VerticalScrollBarEnabled = false;
            }
        }
    }
}


