﻿using System;
using System.ComponentModel;
using Android.Views;
using PETni;
using PETni.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EditorPlaceHolder), typeof(EditorPlaceholderRenderer))]
namespace PETni.Droid
{
    public class EditorPlaceholderRenderer : EditorRenderer
    {
        public EditorPlaceholderRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var element = e.NewElement as EditorPlaceHolder;
                this.Control.Hint = element.Placeholder;
                Control.SetHintTextColor(Color.Gray.ToAndroid());
                Control.SetBackgroundColor(Color.White.ToAndroid());
                Control.SetScrollContainer(true);
                if (!element.IsEnabled)
                {
                    element.IsEnabled = true;
                    Control.Focusable = false;
                    Control.Clickable = true;
                }
                Control.SetOnTouchListener(new MyTouchListener());
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var element = this.Element as EditorPlaceHolder;

            if (e.PropertyName == EditorPlaceHolder.PlaceholderProperty.PropertyName)
            {
                this.Control.Hint = element.Placeholder;
            }
        }
    }

    public class MyTouchListener : Java.Lang.Object, Android.Views.View.IOnTouchListener
    {
        public bool OnTouch(Android.Views.View v, MotionEvent e)
        {
            v.Parent.RequestDisallowInterceptTouchEvent(true);
            switch (e.Action)
            {
                case MotionEventActions.Up:
                    v.Parent.RequestDisallowInterceptTouchEvent(false);
                    break;
            }
            return false;
        }
    }
}