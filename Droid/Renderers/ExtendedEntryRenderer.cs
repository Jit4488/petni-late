﻿using System;
using PETni;
using PETni.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Entry), typeof(ExtendedEntryRenderer))]
namespace PETni.Droid
{
	public class ExtendedEntryRenderer : EntryRenderer
	{
		public ExtendedEntryRenderer()
		{
		}
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
			if (e.NewElement != null)
			{
				Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
				Control.SetPadding(0, 5, 5, 5);
			}
		}
	}
}
