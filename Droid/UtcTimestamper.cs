﻿using System;
using Android.App;
using Android.Content;
using Android.Gms.Common.Apis;
using Android.Gms.Location;
using Android.Locations;
using PETni.Business;

namespace PETni.Droid
{
    public class UtcTimestamper
    {
        DateTime startTime;
        private GoogleApiClient mGoogleApiClient;
        double Latitude = 0, Longitude = 0;
        Location LocationData;

        public UtcTimestamper()
        {
            try
            {
                startTime = DateTime.UtcNow;
                mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.ShareActivityContext)
                                                      .AddApi(LocationServices.API).Build();
                mGoogleApiClient.Connect();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        public string GetFormattedTimestamp()
        {
            try
            {
                TimeSpan duration = DateTime.UtcNow.Subtract(startTime);

                var LocationOn = LocationServices.FusedLocationApi.GetLocationAvailability(mGoogleApiClient);

                if (LocationOn.IsLocationAvailable)
                {
                    LocationData = LocationServices.FusedLocationApi.GetLastLocation(mGoogleApiClient);
                    Latitude = LocationData.Latitude;
                    Longitude = LocationData.Longitude;
                }
                else
                {
                    Latitude = 0;
                    Longitude = 0;
                }

                UpdateLocationRequest item = new UpdateLocationRequest();
                item.id = 0;
                item.request_id = 0;
                item.latitude = Latitude;
                item.longitude = Longitude;
                item.current_date_time = DateTime.Now.ToString("u");
                DBManager<UpdateLocationRequest>.Instance.Insert(item);

                var mLocationRequest = new LocationRequest();

                //LocationServices.FusedLocationApi.RequestLocationUpdates(
                //            mGoogleApiClient, mLocationRequest, getPendingIntent());

                return $"Service started at {DateTime.Now} ({duration:c} ago).";
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
                return "";
            }
        }

        private PendingIntent getPendingIntent()
        {
            try
            {
                Intent intent = new Intent(MainActivity.ShareActivityContext, typeof(MainActivity));
                intent.SetAction(LocationUpdatesIntentService.ACTION_PROCESS_UPDATES);
                return PendingIntent.GetService(MainActivity.ShareActivityContext, 0, intent, PendingIntentFlags.UpdateCurrent);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
                return null;
            }
        }
    }
}
