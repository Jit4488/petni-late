﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Gms.Location;
using Android.Locations;
using Android.Runtime;

namespace PETni.Droid
{
    public class LocationUpdatesIntentService : IntentService
    {
        public LocationUpdatesIntentService()
        {
        }
         
        public static string ACTION_PROCESS_UPDATES = "com.google.android.gms.location.sample.backgroundlocationupdates.action" + ".PROCESS_UPDATES";

        protected override void OnHandleIntent(Intent intent)
        {
            if (intent != null)
            {
                string action = intent.Action;
                if (ACTION_PROCESS_UPDATES.Equals(action))
                {
                    LocationResult result = LocationResult.ExtractResult(intent);
                    if (result != null)
                    {
                        IList<Location> locations = result.Locations;
                        LocationResultHelper locationResultHelper = new LocationResultHelper(this, locations);
                        // Save the location data to SharedPreferences.
                        locationResultHelper.saveResults();
                        // Show notification with the location data.
                        locationResultHelper.showNotification();
                        //Log.i(TAG, LocationResultHelper.getSavedLocationResult(this));
                    }
                }
            }
        }

        //private static string TAG = LocationUpdatesIntentService.class.getSimpleName();

        //public LocationUpdatesIntentService()
        //{
        //    // Name the worker thread.
        //    super(TAG);
        //}
    }
}
