﻿using System;
using System.Collections.Generic;
using System.Text;
using Android.App;
using Android.Content;
using Android.Locations;
using Android.Preferences;
using Android.Text.Format;
using Java.Util;

namespace PETni.Droid
{
    public class LocationResultHelper
    {
        static string KEY_LOCATION_UPDATES_RESULT = "location-update-result";

        private static string PRIMARY_CHANNEL = "default";


        public Context mContext;
        private IList<Location> mLocations;
        private NotificationManager mNotificationManager;

        public LocationResultHelper(Context context, IList<Location> locations)
        {
            mContext = context;
            mLocations = locations;
        }

        /**
         * Returns the title for reporting about a list of {@link Location} objects.
         */
        private string getLocationResultTitle()
        {
            string numLocationsReported = mContext.Resources.GetQuantityString(mLocations.Count, mLocations.Count);
            return numLocationsReported + ": " + DateTime.Now;
        }

        private string getLocationResultText()
        {
            if (mLocations == null || mLocations.Count == 0)
            {
                return mContext.GetString(Resource.Id.right);
            }
            StringBuilder sb = new StringBuilder();
            foreach (Location location in mLocations)
            {
                sb.Append(" (Latitude : ");
                sb.Append(location.Latitude);
                sb.Append(", Longitude ");
                sb.Append(location.Longitude);
                sb.Append(") ");
                sb.Append("\n");
            }
            return sb.ToString();
        }

        ///**
        // * Saves location result as a string to {@link android.content.SharedPreferences}.
        // */
        public void saveResults()
        {
            PreferenceManager.GetDefaultSharedPreferences(mContext)
                             .Edit()
                             .PutString(KEY_LOCATION_UPDATES_RESULT, getLocationResultTitle() + "\n" + getLocationResultText())
                             .Apply();
        }

        ///**
        // * Fetches location results from {@link android.content.SharedPreferences}.
        // */
        static String getSavedLocationResult(Context context)
        {
            return PreferenceManager.GetDefaultSharedPreferences(context)
                                    .GetString(KEY_LOCATION_UPDATES_RESULT, "");
        }

        ///**
        // * Get the notification mNotificationManager.
        // * <p>
        // * Utility method as this helper works with it a lot.
        // *
        // * @return The system service NotificationManager
        // */
        private NotificationManager getNotificationManager()
        {
            if (mNotificationManager == null)
            {
                mNotificationManager = (NotificationManager)mContext.GetSystemService(
                    Context.NotificationService);
            }
            return mNotificationManager;
        }

        ///**
        // * Displays a notification with the location results.
        // */
        public void showNotification()
        {
            Intent notificationIntent = new Intent(mContext, typeof(MainActivity));

            // Construct a task stack.
            TaskStackBuilder stackBuilder = TaskStackBuilder.Create(mContext);

            Activity activity = new Activity();;
            // Add the main Activity to the task stack as the parent.
            stackBuilder.AddParentStack(activity);

            // Push the content Intent onto the stack.
            stackBuilder.AddNextIntent(notificationIntent);

            // Get a PendingIntent containing the entire back stack.
            PendingIntent notificationPendingIntent =
                stackBuilder.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);

            Notification.Builder notificationBuilder = new Notification.Builder(mContext)
                .SetContentTitle(getLocationResultTitle())
                .SetContentText(getLocationResultText())
                //.SetSmallIcon(R.mipmap.ic_launcher)
                .SetSmallIcon(1)
                .SetAutoCancel(true)
                .SetContentIntent(notificationPendingIntent);

            getNotificationManager().Notify(0, notificationBuilder.Build());
        }
    }
}
