﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Facebook;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using FFImageLoading.Forms.Droid;
using Firebase;
using System.Threading.Tasks;
using Firebase.Iid;
using PETni.Business;
using Android.Content.Res;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using static Android.Provider.ContactsContract.Contacts;
using System.Net;

namespace PETni.Droid
{
    [Activity(Label = "PETandI", Icon = "@mipmap/ic_launcher", RoundIcon = "@mipmap/ic_launcher_round", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static MainActivity ShareActivityContext;
        Intent serviceToStart;
        private static Socket socket;
        public bool IsUnsubscirbedFromMessage = true;

        #region fontSize 
        public override Resources Resources
        {
            get
            {
                Resources res = base.Resources;
                Configuration config = new Configuration();
                config.SetToDefaults();
                res.UpdateConfiguration(config, res.DisplayMetrics);
                return res;
            }

        }
        #endregion

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            Rg.Plugins.Popup.Popup.Init(this, bundle);
            base.OnCreate(bundle);
            FacebookSdk.ApplicationId = "2054083061546103";
            FacebookSdk.SdkInitialize(this);
            ShareActivityContext = this;
            CachedImageRenderer.Init(true);

            FFImageLoading.Config.Configuration config = new FFImageLoading.Config.Configuration();
            config.DelayInMs = 1;
            FFImageLoading.ImageService.Instance.Initialize(config);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            Xamarin.FormsMaps.Init(this, bundle);
            //InitSocket();

            LoadApplication(new App());
            Xamarin.Forms.Application.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);

            FirebaseApp app;
            var opt = new FirebaseOptions.Builder()
                                         .SetApplicationId("1:203644954485:android:4a4dd5362fcbbcf0")
                                         .SetApiKey("AIzaSyDuQuyl92ZT_92194o-FXoA5i-muEc5p7A")
                                         .SetGcmSenderId("203644954485")
                                         .Build();
            Task.Run(() =>
            {
                try
                {
                    app = FirebaseApp.GetInstance(FirebaseApp.DefaultAppName);
                    if (app == null)
                    {
                        app = FirebaseApp.InitializeApp(this, opt);
                    }
                    var instanceId = FirebaseInstanceId.Instance;
                    var token = instanceId.GetToken("203644954485", Firebase.Messaging.FirebaseMessaging.InstanceIdScope);

                }
                catch (Exception ex)
                {
                    //ignored
                }
            });

            //serviceToStart = new Intent(this, typeof(LocationService));
            //StartService(serviceToStart);
            InitBroadcast();
        }

        void InitBroadcast()
        {
            try
            {
                TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                long millis = (long)ts.TotalMilliseconds;

                Intent intentAlarm = new Intent(this, typeof(ToastBroadcast));
                AlarmManager alarmManager = (AlarmManager)GetSystemService(Context.AlarmService);
#if DEBUG
                int interval = 3000;
#else
                int interval = 10000;
#endif
                alarmManager.SetRepeating(AlarmType.RtcWakeup, millis, interval, PendingIntent.GetBroadcast(this, 1, intentAlarm, PendingIntentFlags.UpdateCurrent));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        protected override void OnDestroy()
        {
            try
            {
                //Toast.MakeText(ShareActivityContext, string.Format("OnDestroy"), ToastLength.Long).Show();
                //BaseViewModel.SetUserPreference(Enums.KeyBox.ServicesData, Helpers.Serialize(AppSession.Instance.ServiceData));

                App.isAppOpen = false;
                base.OnDestroy();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            var manager = DependencyService.Get<IFacebookManager>();
            if (manager != null)
            {
                (manager as FacebookManager)._callbackManager.OnActivityResult(requestCode, (int)resultCode, data);
            }
        }

        #region Socket
        public void InitSocket()
        {
            if (socket != null)
            {
                socket.Close();
            }


            string url = "http://172.81.117.236:3000";
            socket = IO.Socket(url);
            ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
            socket.On("connect", data =>
            {
                if (data != null)
                {
                    var json = data as JObject;
                    string j = json.ToString(Newtonsoft.Json.Formatting.None);
                    Data d = JsonConvert.DeserializeObject<Data>(j);
                    //ChatMessageRestService.cmrr = d.data;
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        //System.Diagnostics.Debug.WriteLine(ex);
                        Logger.SendErrorLog(ex);
                    }
                    finally
                    {
                        //ProgressDialog.HideProgress();
                    }
                }
            });

            socket.Connect();
            socket.On(Socket.EVENT_ERROR, (obj) =>
            {

            });
            socket.On(Socket.EVENT_CONNECT_ERROR, (obj) =>
            {

            });
            socket.On(Socket.EVENT_RECONNECT_ERROR, (obj) =>
            {

            });
            socket.On(Socket.EVENT_CONNECT_TIMEOUT, (obj) =>
            {

            });
            socket.On(Socket.EVENT_RECONNECT_FAILED, (obj) =>
            {

            });
            socket.On(Socket.EVENT_CONNECT, (obj) =>
            {
            });

            if (IsUnsubscirbedFromMessage)
            {
                SubscribeForMessages();
            }
        }

        public void SubscribeForMessages()
        {
            try
            {
                MessagingCenter.Subscribe<UpdateLocationRequest, string>(this, "UpdateLocation", (sender, arg) =>
                {
                    var data = JsonConvert.DeserializeObject<UpdateLocationRequest>(arg);
                    var storedate = BaseViewModel.GetUserPreference(Enums.KeyBox.LocationSendData);
                    if (storedate == null)
                    {
                        socket.Emit("location", arg);
                        IsUnsubscirbedFromMessage = false;
                        BaseViewModel.SetUserPreference(Enums.KeyBox.LocationSendData, data.current_date_time);
                    }
                    else if (storedate != data.current_date_time)
                    {
                        socket.Emit("location", arg);
                        IsUnsubscirbedFromMessage = false;
                        BaseViewModel.SetUserPreference(Enums.KeyBox.LocationSendData, data.current_date_time);
                    }
                });
            }
            catch (Exception ex)
            {
                //System.Diagnostics.Debug.WriteLine(ex);
                Logger.SendErrorLog(ex);
            }
        }
        #endregion
    }
}
