﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoadingPopup : PopupPage
    {
        public LoadingPopup()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            //loader.Source = null;
        }
    }
}
