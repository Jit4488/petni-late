﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NetworkPopupPage : PopupPage
    {
        public NetworkPopupPage()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();
                BaseViewModel.IsPopupOpen = false;
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        async void RetryTapped(object sender, System.EventArgs e)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    if (BaseViewModel.IsPopupOpen)
                    {
                        await PopupNavigation.PopAsync();
                        BaseViewModel.IsPopupOpen = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
    }
}
