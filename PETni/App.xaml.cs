﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PETni.Business;
using Plugin.Geolocator;
using Plugin.LocalNotifications;
using Xamarin.Forms;

namespace PETni
{
    public partial class App : Application
    {
        #region CommonProperty
        public static IUserPreferences userPreferences = DependencyService.Get<IUserPreferences>();
        public static IUserRepository userRepository = RepositoryInitiator<IUserRepository>.Instance;
        public static int count = 0;
        public static bool isAppOpen = false;
        #endregion

        #region Property
        public static INavigation Navigation { get; set; }
        public static Page CurrentPage { get; set; }
        #endregion

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new SplashScreen());
            Navigation = MainPage.Navigation;
            //setMainPage();

            //CrossLocalNotifications.Current.Show("PETni", "Local Notification", 0, DateTime.Now.AddSeconds(5));
        }

        protected override void OnStart()
        {
            //Handle when your app starts
            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (NetworkProvider.IsAvailable())
                    {
                        var LocationList = new List<UpdateLocationRequest>();
                        var LocationStr = BaseViewModel.GetUserPreference(Enums.KeyBox.LocationData);
                        if (LocationStr != null)
                            LocationList = Helpers.Deserialize<List<UpdateLocationRequest>>(LocationStr);

                        //await Utilities.ShowOkAlertAsync("Location : " + LocationStr);

                        var ServiceList = new List<BackServiceDetails>();
                        var ServiceStr = BaseViewModel.GetUserPreference(Enums.KeyBox.ServicesData);
                        if (ServiceStr != null)
                            ServiceList = Helpers.Deserialize<List<BackServiceDetails>>(ServiceStr);

                        //await Utilities.ShowOkAlertAsync("Service : " + ServiceStr);

                        //----------Update Location Data--------//
                        if (LocationList != null && LocationList.Count > 0)
                        {
                            AppSession.Instance.LocationData = new List<UpdateLocationRequest>();
                            foreach (var item in ServiceList)
                            {
                                AppSession.Instance.LocationData.Add(new UpdateLocationRequest { id = item.id, request_id = item.request_id, latitude = 0, longitude = 0, current_date_time = DateTime.Now.ToString("u") });
                            }
                            foreach (var item in LocationList)
                            {
                                BaseViewModel.UpdateLocation(item.id, item.request_id);
                            }
                        }

                        //--------Start Time for Update Location------//
                        if (ServiceList != null && ServiceList.Count > 0)
                        {
                            foreach (var item in ServiceList)
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    Task.Run(() =>
                                    {
                                        var timer = new Timer((e) =>
                                        {
                                            BaseViewModel.UpdateLocation(item.id, item.request_id);
                                        }, null, TimeSpan.Zero, TimeSpan.FromSeconds(30));
                                    });
                                });
                            }
                        }

                        BaseViewModel.SetUserPreference(Enums.KeyBox.LocationData, null);
                        //BaseViewModel.SetUserPreference(Enums.KeyBox.ServicesData, null);

                        //LocationUpdate();
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            //InsertLocationData();

            //BaseViewModel.SetUserPreference(Enums.KeyBox.LocationData, Helpers.Serialize(LocationList));
            //CrossLocalNotifications.Current.Show(Constants.APP_NAME, "Location update", 1, Next(DateTime.Now));

            //LocationUpdate();
            isAppOpen = false;
        }

        public void LocationUpdate()
        {
            void ScheduleNotification()
            {
                // Start a timer that runs after 5 seconds.
                Device.StartTimer(TimeSpan.FromSeconds(5), () =>
                {
                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        // Do the actual request and wait for it to finish.
                        PerformNotification();
                        // Switch back to the UI thread to update the UI
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            // Update the UI
                            // ...
                            // Now repeat by scheduling a new request
                            ScheduleNotification();
                            count++;
                        });
                    }, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);

                    // Don't repeat the timer (we will start a new timer when the request is finished)
                    return false;
                });
            }
            void PerformNotification()
            {
                CrossLocalNotifications.Current.Show(Constants.APP_NAME, "Location update " + count.ToString());
                //CrossLocalNotifications.Current.Show(Constants.APP_NAME, "Location update", 2, App.Next(DateTime.Now, DayOfWeek.Sunday));
            }
            ScheduleNotification();
        }

        public static DateTime Next(DateTime from, DayOfWeek dayOfWeek)
        {
            //int start = (int)from.DayOfWeek;
            //int target = (int)dayOfWeek;
            //if (target <= start)
            //target += 7;
            //return from.AddDays(target - start);
            return from.AddSeconds(5);
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            //GetLocationData();
            isAppOpen = true;
        }

        public static void setMainPage()
        {
            //MainPage = new NavigationPage(new SplashScreen());

            var id = BaseViewModel.GetUserPreference(Enums.KeyBox.UserId);
            if (id != null && !string.IsNullOrEmpty(id))
            {
                App.Current.MainPage = new NavigationPage(new HomePage());
                Navigation = App.Current.MainPage.Navigation;
            }
            else
            {
                App.Current.MainPage = new NavigationPage(new LoginPage());
                Navigation = App.Current.MainPage.Navigation;
            }
        }

        //public void InsertLocationData()
        //{
        //    try
        //    {
        //        if (LocationList != null && LocationList.Count > 0)
        //        {
        //            foreach (var item in LocationList)
        //            {
        //                var timer = new System.Threading.Timer(async (e) =>
        //                {
        //                    if (NetworkProvider.IsAvailable())
        //                    {
        //                        var locator = CrossGeolocator.Current;
        //                        locator.DesiredAccuracy = 50;

        //                        double Latitude = 0, Longitude = 0;

        //                        if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
        //                        {
        //                            var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

        //                            Latitude = position.Latitude;
        //                            Longitude = position.Longitude;
        //                        }
        //                        else
        //                        {
        //                            ProgressDialog.HideProgress();
        //                            Utilities.ShowOkAlert("To continue, let your device turn on location using Google's location service");
        //                            return;
        //                        }

        //                        item.latitude = Latitude;
        //                        item.longitude = Longitude;
        //                        item.current_date_time = DateTime.Now;
        //                        DBManager<UpdateLocationRequest>.Instance.Insert(item);
        //                    }
        //                }, null, TimeSpan.Zero, TimeSpan.FromSeconds(30));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.SendErrorLog(ex);
        //    }
        //}

        //public void GetLocationData()
        //{
        //    try
        //    {
        //        if (NetworkProvider.IsAvailable())
        //        {
        //            LocationList = new List<UpdateLocationRequest>();
        //            var LocationData = DBManager<UpdateLocationRequest>.Instance.GetAll();
        //            if (LocationData != null && LocationData.Count > 0)
        //            {
        //                LocationList = LocationData.GroupBy(x => new { x.id, x.request_id }).Select(x => x.FirstOrDefault()).ToList();
        //                //foreach (var item in LocationData)
        //                //{
        //                //    BaseViewModel.UpdateLocation(item.id, item.request_id);
        //                //}
        //                //DBManager<UpdateLocationRequest>.Instance.DeleteAll();

        //                //foreach (var item in LocationList)
        //                //{
        //                //    var timer = new System.Threading.Timer((e) =>
        //                //    {
        //                //        BaseViewModel.UpdateLocation(item.id, item.request_id);
        //                //    }, null, TimeSpan.Zero, TimeSpan.FromSeconds(30));
        //                //}
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.SendErrorLog(ex);
        //    }
        //}
    }
}
