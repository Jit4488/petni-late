﻿using System;
namespace PETni
{
	public class ChatMessage
	{
		public bool IsInChatMessage { get; set; }
		public string Message { get; set; }
		public string senderId { get; set; }
		public string receiverId { get; set; }
		public string taskId { get; set; }
		public DateTime date { get; set; }
		public string Key { get; set; }
	}
}
