﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace PETni
{
    /// <summary>
    /// Progress dialog
    /// </summary>
    public static class ProgressDialog
    {
        public static bool isPopUpOpen = false;
        public static void ShowProgress(String title = "Loading...")
        {
            try
            {
                if (isPopUpOpen)
                    return;
                PopupPage page;
                page = new LoadingPopup();
                page.CloseWhenBackgroundIsClicked = false;
                PopupNavigation.PushAsync(page);
                isPopUpOpen = true;
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                //ProgressDialog.HideProgress();
            }
        }

        public async static void HideProgress()
        {
            try
            {
                //if (isPopUpOpen)
                //{

                //Device.BeginInvokeOnMainThread(() =>
                //{
#if __ANDROID__
                await Task.Delay(5000);
#endif
                await PopupNavigation.PopAllAsync();
                isPopUpOpen = false;

                if (NetworkProvider.IsAvailable())
                {
                    if (BaseViewModel.IsPopupOpen)
                    {

                    }
                }
                //});


                //}
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                //ProgressDialog.HideProgress();
            }
            //DependencyService.Get<IProgressDialog>().Dismiss();
        }

        public static void ShowToastMessage(String title = "Loading...")
        {
            DependencyService.Get<IProgressDialog>().Show(title);
        }

        public static void ShowProgressWithMessage(string message, int completed, int Total)
        {
            DependencyService.Get<IProgressDialog>().ShowProgressWithMessage(message, completed, Total);
        }
    }

    /// <summary>
    /// User interface thread.
    /// </summary>
    public static class UIThread
    {
        public async static void BeginInvokeOnMainThreadAsync(Action action)
        {
            await MainThreadAsync(action);
        }

        private static Task MainThreadAsync(Action action)
        {
            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    action();
                    tcs.SetResult(null);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }
    }
}
