﻿using System;
using System.Collections.Generic;
using System.Linq;
using PETni.Business;
using SQLite;
using Xamarin.Forms;

namespace PETni
{
	public partial class DBManager<T> where T : new()
    {
        public DBManager()
        {
            Connection = DependencyService.Get<ISQLiteHelper>().GetConnection();
            CreateTables();
        }

        private SQLiteConnection Connection;
        public object SyncObject = new object();

        private static DBManager<T> _instance;
        public static DBManager<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DBManager<T>();
                }
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        private static Type[] DatabaseTables = new Type[] {
            typeof(UpdateLocationRequest)
        };

        public void CreateTables()
        {
            foreach (var tableType in DatabaseTables)
            {
                Connection.CreateTable(tableType);
            }
        }

        public int Insert(T entity)
        {
            return Connection.Insert(entity);
        }

        public int InsertOrReplace(T entity)
        {
            lock (SyncObject)
            {
                return Connection.InsertOrReplace(entity);
            }
        }

        public int Update(T entity)
        {
            lock (SyncObject)
            {
                return Connection.Update(entity);
            }
        }

        public void InsertAll(List<T> entity)
        {
            lock (SyncObject)
            {
                Connection.InsertAll(entity);
            }
        }

        public void InsertOrReplaceAll(List<T> entity)
        {
            foreach (var item in entity)
            {
                Connection.InsertOrReplace(item);
            }
        }


        public void DeleteAll()
        {
            lock (SyncObject)
            {
                Connection.DeleteAll<T>();
            }
        }

        public void DeleteById(int Id)
        {
            lock (SyncObject)
            {
                Connection.Delete<T>(Id);
            }
        }

        public List<T> GetAll()
        {
            return Connection.Table<T>().ToList();
        }

    }
}
