﻿using System;
using PETni.Business;
using Plugin.Connectivity;
using Xamarin.Forms;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;

namespace PETni
{
    public static class NetworkProvider
    {
        public static bool IsAvailable()
        {
            var connectivity = CrossConnectivity.Current;

            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    if (!connectivity.IsConnected)
                    {
                        if (BaseViewModel.IsPopupOpen)
                            return;
                        BaseViewModel.IsPopupOpen = true;

                        PopupPage page = new NetworkPopupPage();
                        page.CloseWhenBackgroundIsClicked = false;
                        await PopupNavigation.PushAsync(page, true);
                    }
                }
                catch (Exception ex)
                {
                    Logger.SendErrorLog(ex);
                }
            });

            return connectivity.IsConnected;
        }
    }
}
