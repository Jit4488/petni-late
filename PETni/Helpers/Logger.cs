﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace PETni
{
    public static class Logger
    {
        public static void SendErrorLog(this Exception ex)
        {
            Console.Write(ex);
            ProgressDialog.HideProgress();
#if DEBUG
            try
            {
                Utilities.ShowOkAlert(ex.Message);
            }
            catch (Exception exc)
            {

            }
            finally
            {
                ProgressDialog.HideProgress();
            }
#endif
            //Insights.Report(ex, Insights.Severity.Error);
        }

        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable enumerable)
        {
            return new ObservableCollection<T>(enumerable.Cast<T>());
        }
    }
}
