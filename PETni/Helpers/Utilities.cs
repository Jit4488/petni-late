﻿using System;
using System.IO;
using System.Threading.Tasks;
using PETni.Business;
using Xamarin.Forms;

namespace PETni
{
    public class Utilities
    {
        public static void ShowOkAlert(string message)
        {
            if (!String.IsNullOrEmpty(message))
            {
                Application.Current.MainPage.DisplayAlert(Constants.AlertTitle, message, Constants.OkButtonText);
            }
        }
        public static async Task ShowOkAlertAsync(string message)
        {
            await Application.Current.MainPage.DisplayAlert(Constants.AlertTitle, message, Constants.OkButtonText);
        }

        public static async Task<bool> ShowOkCancelAlert(string message)
        {
            if (!String.IsNullOrEmpty(message))
            {
                var answer = await Application.Current.MainPage.DisplayAlert(Constants.APP_NAME, message, "Ok", "Cancel");
                return answer;
            }
            return false;
        }

        public static byte[] ToByteArray(Stream stream)
        {
            stream.Position = 0;
            byte[] buffer = new byte[stream.Length];
            for (int totalBytesCopied = 0; totalBytesCopied < stream.Length;)
                totalBytesCopied += stream.Read(buffer, totalBytesCopied, Convert.ToInt32(stream.Length) - totalBytesCopied);
            return buffer;

        }

    }
}

