﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace PETni
{
    public class FrameHeightConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value is double && value != null)
            {
                double s = (double)value;
                if (s > 0)
                    return (s / 2 < 25) ? s / 2 : 15;
            }
            return 15;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
