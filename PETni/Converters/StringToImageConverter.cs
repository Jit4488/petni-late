﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace PETni
{
    public class StringToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string && value != null)
            {
                if ((string)value == "Morning")
                {
                    return "tea_icon.png";
                }
                if ((string)value == "Afternoon")
                {
                    return "afternoon_icon.png";
                }
                if ((string)value == "Evening")
                {
                    return "afternoon_icon.png";
                }
                if ((string)value == "All Day")
                {
                    return "afternoon_icon.png";
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
