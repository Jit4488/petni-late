﻿using System;
using System.Collections;
using System.Globalization;
using Xamarin.Forms;

namespace PETni
{
    public class EmptyListViewHelper : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is IEnumerable)
            {
                if (((IEnumerable)value).ToObservableCollection<object>().Count > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
