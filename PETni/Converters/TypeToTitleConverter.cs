﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace PETni
{
    public class TypeToTitleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string && value != null)
            {
                if ((string)value == "pee")
                {
                    return "Peed";
                }
                if ((string)value == "poo")
                {
                    return "Pooped";
                }
                if ((string)value == "nap")
                {
                    return "Napped";
                }
                if ((string)value == "food")
                {
                    return "had a Food";
                }
                if ((string)value == "walk")
                {
                    return "had a Walk";
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
