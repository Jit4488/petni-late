﻿using System;
using PETni.Business;
using Xamarin.Forms;

namespace PETni
{
    public class ChatDataTemplateSelector : DataTemplateSelector
    {
        #region Variable declaration

        private readonly DataTemplate inMessageDataTemplate;
        private readonly DataTemplate outMessageDataTemplate;

        #endregion

        #region Constructor

        public ChatDataTemplateSelector()
        {
            this.inMessageDataTemplate = new DataTemplate(typeof(InMessageViewCell));
            this.outMessageDataTemplate = new DataTemplate(typeof(OutMessageViewCell));
        }

        #endregion

        #region Methods

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (!(item is MessageDetail))
                return null;

            var userID = Convert.ToInt32(BaseViewModel.GetUserPreference(Enums.KeyBox.UserId));

            return ((MessageDetail)item).user_id != userID ? this.inMessageDataTemplate : this.outMessageDataTemplate;
        }

        #endregion
    }
}