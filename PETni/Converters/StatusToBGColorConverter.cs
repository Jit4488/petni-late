﻿using System;
using System.Globalization;
using Xamarin.Forms;
namespace PETni
{
    public class StatusToBGColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int && value != null)
            {
                if ((int)value == 1)
                {
                    return Xamarin.Forms.Color.FromHex("#de5b26");
                }
                else
                    return Xamarin.Forms.Color.FromHex("#e5eef4");
            }
            else
                return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
