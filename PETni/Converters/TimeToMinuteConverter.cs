﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace PETni
{
    public class TimeToMinuteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime && value != null)
            {
                DateTime time = System.Convert.ToDateTime(value);
                return time.ToString("t");
                //DateTime s = (DateTime)value;
                //double DiffMin = (DateTime.Now).Subtract(s).TotalMinutes;
                //if (System.Convert.ToInt32(DiffMin) == 0)
                //	return "Now";
                //else if (DiffMin < 60)
                //	return System.Convert.ToInt32(DiffMin) + " M ago";
                //else if (DiffMin < 60 * 24 && DiffMin > 60)
                //	return System.Convert.ToInt32((DateTime.Now).Subtract(s).TotalHours) + " Hr ago";
                //else
                //return System.Convert.ToInt32((DateTime.Now).Subtract(s).TotalDays) + " Days ago";
            }
            else if (value is string && value != null && !string.IsNullOrEmpty(value.ToString()))
            {
                DateTime s = System.Convert.ToDateTime(value.ToString());
                double DiffMin = (DateTime.Now).Subtract(s).TotalMinutes;
                if (System.Convert.ToInt32(DiffMin) == 0)
                    return "Now";
                else if (DiffMin < 60)
                    return System.Convert.ToInt32(DiffMin) + " M ago";
                else if (DiffMin < 60 * 24 && DiffMin > 60)
                    return System.Convert.ToInt32((DateTime.Now).Subtract(s).TotalHours) + " Hr ago";
                else
                    return System.Convert.ToInt32((DateTime.Now).Subtract(s).TotalDays) + " Days ago";
            }
            else
                return "a while ago";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

