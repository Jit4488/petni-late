﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using PETni.Business;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ServiceStartPage : ContentPage
    {
        int ServiceId, ServiceDuration;
        public ServiceStartPage(int id, ObservableCollection<IndividualServiceDetail> ServiceList, int service_duration)
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);
                ServiceId = id;
                ServiceDuration = service_duration;
                this.BindingContext = new ServiceViewModel(ServiceList, ServiceDuration);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            (this.BindingContext as ServiceViewModel).getServicesDetail(ServiceId);
        }
    }
}