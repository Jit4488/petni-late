﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using System.Collections.ObjectModel;
//using PETni.Business;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CareInstructionPage : ContentPage
    {
        string Name;
        int petId;
        public CareInstructionPage(int id, string name, List<Business.PetsDetail> PetDetails)
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);
                petId = id;
                //petId = 138;
                Name = name;
                this.BindingContext = new CareInstructionPageViewModel(Name, PetDetails);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            (this.BindingContext as CareInstructionPageViewModel).getPetCareDetails(petId);
        }

        void Handle_Tapped(object sender, System.EventArgs e)
        {
            var image = sender as Image;
            string uri = image.Source.ToString().Replace("Uri:", "");
            Device.OpenUri(new Uri(uri));
        }
    }
}
