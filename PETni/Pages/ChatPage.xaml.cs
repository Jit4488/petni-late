﻿using PETni.Business;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatPage : ChatContentPage
    {
        int ReqId;
        int ID;
        string StartTime;
        string ServiceName;
        int Isdone, ServiceDuration;

        public ChatPage(int id, int requestId, string startTime, string serviceName, int IsDone, string endTime, int serviceDuration)
        {
            try
            {
                InitializeComponent();
                ReqId = requestId;
                ID = id;
                StartTime = startTime;
                Isdone = IsDone;
                ServiceDuration = serviceDuration;
                NavigationPage.SetHasNavigationBar(this, false);

                //TimeSpan DiffMin = (!startTime.IsEmpty()) ? (DateTime.Now).Subtract(Convert.ToDateTime(StartTime)) : TimeSpan.FromMinutes(0);
                TimeSpan DiffMin = (!startTime.IsEmpty()) ? (Convert.ToDateTime(DateTime.Now.ToString("HH:mm:ss tt"))).Subtract(Convert.ToDateTime(StartTime)) : TimeSpan.FromMinutes(0);
                if (!Helpers.IsEmpty(endTime))
                {
                    DiffMin = Convert.ToDateTime(endTime).TimeOfDay;
                }
                base.BindingContext = new ChatViewModel(ID, requestId, DiffMin, this, IsDone, StartTime, ServiceDuration);
                ServiceName = serviceName;
                if (serviceName == "Dog Walking")
                {
                    DogWalking.IsVisible = true;
                    PetSitting.IsVisible = false;
                }
                else
                {
                    DogWalking.IsVisible = false;
                    PetSitting.IsVisible = true;
                }
                if (Isdone == 0)
                {
                    EnableDone.Source = "right_round";
                    enabledone.Source = "right_round";
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                (this.BindingContext as ChatViewModel).previousMessage(ID, ReqId);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        public void UpdateUI()
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if ((base.BindingContext as ChatViewModel).ChatMessages != null && (base.BindingContext as ChatViewModel).ChatMessages.Count > 0)
                    {
                        var lstItem = (base.BindingContext as ChatViewModel).ChatMessages.Last();
                        ChatList.ScrollTo(lstItem, ScrollToPosition.End, false);

                    }
                });
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        void Done_Tapped(object sender, System.EventArgs e)         {
            if (Isdone == 0)
            {
                SeviceDone.IsVisible = true;
                DogWalking.IsVisible = false;
                PetSitting.IsVisible = false;
                (this.BindingContext as ChatViewModel).CurrentTimeExecute();
            }         }          void NotYet_Tapped(object sender, System.EventArgs e)         {             if (ServiceName == "Dog Walking")             {                 SeviceDone.IsVisible = false;                 DogWalking.IsVisible = true;
                (this.BindingContext as ChatViewModel).TxtReason = string.Empty;
                (this.BindingContext as ChatViewModel).IsVisibleSeviceDoneReasonGrid = false;             }             else             {                 SeviceDone.IsVisible = false;                 PetSitting.IsVisible = true;
                (this.BindingContext as ChatViewModel).TxtReason = string.Empty;
                (this.BindingContext as ChatViewModel).IsVisibleSeviceDoneReasonGrid = false;             }         }

        void tea_Tapped(object sender, System.EventArgs e)
        {
            (this.BindingContext as ChatViewModel).Type = "poo";
            OtherOptionSelected("tea");
        }

        void blue_icon_Tapped(object sender, System.EventArgs e)
        {
            (this.BindingContext as ChatViewModel).Type = "pee";
            OtherOptionSelected("blue_icon");
        }

        void food_Tapped(object sender, System.EventArgs e)
        {
            (this.BindingContext as ChatViewModel).Type = "food";
            OtherOptionSelected("food");
        }

        void foot_mark_Tapped(object sender, System.EventArgs e)
        {
            (this.BindingContext as ChatViewModel).Type = "walk";
            OtherOptionSelected("foot_mark");
        }

        void sleep_Tapped(object sender, System.EventArgs e)
        {
            (this.BindingContext as ChatViewModel).Type = "nap";
            OtherOptionSelected("sleep");
        }

        //#if __IOS__
        //            var resourcePrefix = "PETni.iOS";
        //#endif
        //#if __ANDROID__
        //            var resourcePrefix = "PETni.Droid";
        //#endif
        //#if WINDOWS_PHONE
        //                        var resourcePrefix = "PETni.WinPhone";
        //#endif

        //var assembly = typeof(ChatPage).GetTypeInfo().Assembly;
        //Stream stream = assembly.GetManifestResourceStream($"{resourcePrefix}.1.jpg");

        //var bytes = Utilities.ToByteArray(stream);

        //(this.BindingContext as ChatViewModel).profileImageByte = bytes;
        //await (this.BindingContext as ChatViewModel).sendMessage();



        async void OtherOptionSelected(string Service)
        {
#if __IOS__
            var resourcePrefix = "PETni.iOS";
#endif
#if __ANDROID__
            var resourcePrefix = "PETni.Droid";
#endif
#if WINDOWS_PHONE
                        var resourcePrefix = "PETni.WinPhone";
#endif

            var assembly = typeof(ChatPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream($"{resourcePrefix}." + Service + ".jpg");

            var bytes = Utilities.ToByteArray(stream);

            (this.BindingContext as ChatViewModel).profileImageByte = bytes;
            await (this.BindingContext as ChatViewModel).sendMessage();

        }



        void Yes_Tapped(object sender, System.EventArgs e)
        {
            if (ServiceName == "Dog Walking")
            {
                //DoneWithService.IsVisible = false;
                DogWalking.IsVisible = true;
            }
            else
            {
                //DoneWithService.IsVisible = false;
                PetSitting.IsVisible = true;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            App.Navigation.PushAsync(new HomePage());
            return true; //base.OnBackButtonPressed();
        }
    }
}
