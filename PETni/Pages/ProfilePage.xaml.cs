﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PETni.Business;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage(ObservableCollection<PetsProfile> petsProfile)
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);
                this.BindingContext = new ProfilePageViewModel(petsProfile);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        void Handle_Tapped(object sender, System.EventArgs e)
        {
            var image = sender as Xamarin.Forms.Image;
            string uri = image.Source.ToString().Replace("Uri:", "");
            Device.OpenUri(new Uri(uri));
        }
    }
}
