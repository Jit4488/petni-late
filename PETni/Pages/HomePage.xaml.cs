﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PETni.Business;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            try
            {
                InitializeComponent();
                this.BindingContext = new HomeViewModel();
                NavigationPage.SetBackButtonTitle(this, "Back");
                NavigationPage.SetHasNavigationBar(this, false);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {

            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            (this.BindingContext as HomeViewModel).getServiceList(DateTime.Today.ToString("MMMM dd, yyyy"));
        }

        void GotoMap_Tapped(object sender, System.EventArgs e)
        {
            try
            {
                var Data = ((e as TappedEventArgs).Parameter as IndividualServiceDetail).customer_address;
                var lat = Data.latitude;
                var lon = Data.longitude;
                var address = Data.address + ", " + Data.address_2 + ", " + Data.city + ", " + Data.state + ", " + Data.pincode;
                Navigation.PushAsync(new MapPage(lat, lon, address));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
    }
}