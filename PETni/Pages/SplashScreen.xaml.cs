﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading;
using PETni.Business;
using Plugin.Geolocator;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashScreen : ContentPage
    {
        public SplashScreen()
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            Device.BeginInvokeOnMainThread(Redirect);
        }

        public async void Redirect()
        {

            //var id = BaseViewModel.GetUserPreference(Enums.KeyBox.UserId);
            //if (id != null && !string.IsNullOrEmpty(id))
            //{
            //    await Task.Delay(TimeSpan.FromSeconds(5));
            //    await App.Navigation.PushAsync(new HomePage());
            //    BaseViewModel.RemovePreviousPage();
            //}
            //else
            //{
            //    await Task.Delay(TimeSpan.FromSeconds(5));
            //    await App.Navigation.PushAsync(new LoginPage());
            //    BaseViewModel.RemovePreviousPage();
            //}

            await Task.Delay(TimeSpan.FromSeconds(5));
            //await App.Navigation.PushAsync(new LoginPage());
            App.setMainPage();
            //BaseViewModel.RemovePreviousPage();
        }
    }
}
