﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPage : ContentPage
    {
        Pin CurrentLocationPin;

        public MapPage(string lat, string lon, string address)
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetBackButtonTitle(this, "Back");

                CurrentLocationPin = new Pin();
                CurrentLocationPin.Type = PinType.Place;
                CurrentLocationPin.Label = address;
                MyMa.MapType = MapType.Street;
                CurrentLocationPin.Position = new Position(Convert.ToDouble(lat), Convert.ToDouble(lon));
                MyMa.Pins.Add(CurrentLocationPin);
                MyMa.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(Convert.ToDouble(lat), Convert.ToDouble(lon)), Distance.FromKilometers(1)));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
    }
}
