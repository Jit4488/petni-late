﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PETni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InstructionPage : ContentPage
    {
        string DifferenceMinitues, Reason;
        int ID;
        public InstructionPage(string differenceMinitues, int Id, string reason)
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);
                DifferenceMinitues = differenceMinitues;
                ID = Id;
                Reason = reason;
                this.BindingContext = new InstructionViewModel(DifferenceMinitues, Id, Reason);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                //int ServiceId = 1;
                (this.BindingContext as InstructionViewModel).getServicesDetail(ID);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
    }
}