﻿using System;
using Xamarin.Forms;

namespace PETni
{
    public class EditorPlaceHolder : Editor
    {
        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create<EditorPlaceHolder, string>(view => view.Placeholder, String.Empty);

        public string Placeholder
        {
            get
            {
                return (string)GetValue(PlaceholderProperty);
            }

            set
            {
                SetValue(PlaceholderProperty, value);
            }
        }
    }
}
