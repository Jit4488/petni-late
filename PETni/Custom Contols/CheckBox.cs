﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace PETni
{
    public class CheckBox : Grid
    {

        Image CheckboxImage;
        Label CheckboxText;

        public CheckBox()
        {
            this.Style = Application.Current.Resources["CheckBoxStyle"] as Style;
            Padding = new Thickness(0, 3, 0, 3);
            CheckboxImage = new Image();
            CheckboxText = new Label();
            CheckboxImage.VerticalOptions = LayoutOptions.CenterAndExpand;
            CheckboxText.VerticalTextAlignment = TextAlignment.Center;
            CheckboxImage.Source = ImageSource.FromFile("round.png");
            GestureRecognizers.Add(new TapGestureRecognizer(sender =>
            {
                if (IsEnabled)
                    Ischecked = !Ischecked;
                if (sender != null)
                {
                    var v = sender.BindingContext;
                    if (Command != null)
                    {
                        Command.Execute(v);
                    }
                }
                else
                {
                    if (Command != null)
                    {
                        Command.Execute(Ischecked);
                    }
                }
            }));


            ColumnDefinitions.Add(new ColumnDefinition()
            {
                Width = new GridLength(30)
            });

            ColumnDefinitions.Add(new ColumnDefinition()
            {
                Width = new GridLength(1, GridUnitType.Auto)
            });
            this.Children.Add(CheckboxImage, 0, 0);
            this.Children.Add(CheckboxText, 1, 0);
        }

        public static readonly BindableProperty IscheckedProperty =
            BindableProperty.Create<CheckBox, bool>(p => p.Ischecked, false,
                BindingMode.TwoWay, null,
                new BindableProperty.BindingPropertyChangedDelegate<bool>(IsCheckedChanged), null, null);

        private static void IsCheckedChanged(BindableObject bindable, bool oldValue, bool newValue)
        {
            CheckBox Control = (bindable as CheckBox);
            Control.CheckboxImage.Source = Control.Ischecked ? "rignt_icon_2.png" : "round.png";
        }

        public bool Ischecked
        {
            get
            {
                return (bool)GetValue(IscheckedProperty);
            }
            set
            {
                SetValue(IscheckedProperty, value);
            }
        }

        public static readonly BindableProperty TextProperty =
            BindableProperty.Create<CheckBox, string>(p => p.Text, "",
                BindingMode.TwoWay, null,
                new BindableProperty.BindingPropertyChangedDelegate<string>(TextPropertyChanged), null, null);

        static void TextPropertyChanged(BindableObject bindable, string oldValue, string newValue)
        {
            CheckBox Control = (bindable as CheckBox);
            Control.CheckboxText.Text = newValue;

        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create<CheckBox, Color>(p => p.TextColor, Color.Gray,
                BindingMode.TwoWay, null,
                new BindableProperty.BindingPropertyChangedDelegate<Color>(TextColorPropertyChanged), null, null);

        static void TextColorPropertyChanged(BindableObject bindable, Color oldValue, Color newValue)
        {
            CheckBox Control = (bindable as CheckBox);
            Control.CheckboxText.TextColor = newValue;
        }

        public Color TextColor
        {
            get
            {
                return (Color)GetValue(TextColorProperty);
            }
            set
            {
                SetValue(TextColorProperty, value);
            }
        }

        #region command property and its method

        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create<CheckBox, ICommand>(c => c.Command, null);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly BindableProperty CommandParameterProperty =
            BindableProperty.Create<CheckBox, object>(c => c.CommandParameter, null);

        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        #endregion
    }
}
