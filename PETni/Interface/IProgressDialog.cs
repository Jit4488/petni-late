﻿using System;
namespace PETni
{
    public interface IProgressDialog
    {
        void Show();

        bool IsShowing { get; set; }

        void Show(string message);

        void ShowToastMessage(string message);

        void ShowProgressWithMessage(string message, int completed, int Total);

        void CloseKeyBoard();

        void Dismiss();
    }
}