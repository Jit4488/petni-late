﻿using System;
using SQLite;

namespace PETni
{
    public interface ISQLiteHelper
    {
        SQLiteConnection GetConnection();
    }
}

