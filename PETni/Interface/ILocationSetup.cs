﻿using System;
using System.Threading.Tasks;
namespace PETni
{
    public interface ILocationSetup
    {
        bool SetLocation();
    }
}
