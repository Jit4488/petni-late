﻿using System;
using System.Threading.Tasks;

namespace PETni
{
    public interface ISocialAuth
    {
        Task TwitterLogin(bool allowCancel, BaseViewModel vm);
    }


    public interface IFacebookManager
    {
        void Login(Action<FacebookUser, string> onLoginComplete);
        void Logout();
    }
}