﻿using PETni.Business;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.IO;
using System.Linq;
using System.Globalization;
using Plugin.Geolocator;

namespace PETni
{
    public class ChatViewModel : BaseViewModel
    {
        #region property
        public ChatPage chatPg;
        string Extension, StartTime;
        int Id, ServiceDuration, LastMinutes;
        double Latitude = 0, Longitude = 0;

        private bool isVisibleSeviceDoneReasonGrid = false;
        public bool IsVisibleSeviceDoneReasonGrid
        {
            get { return isVisibleSeviceDoneReasonGrid; }
            set { isVisibleSeviceDoneReasonGrid = value; OnPropertyChanged(); }
        }

        private string txtReason;
        public string TxtReason
        {
            get { return txtReason; }
            set { txtReason = value; OnPropertyChanged(); }
        }

        public Byte[] profileImageByte;
        private string type;
        public string Type
        {
            get { return type; }
            set { type = value; OnPropertyChanged(); }
        }
        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; OnPropertyChanged(); }
        }
        private bool isRunning = true;
        public bool IsRunning
        {
            get { return isRunning; }
            set { isRunning = value; OnPropertyChanged(); }
        }
        private ImageSource profileImage = "user";
        public ImageSource ProfileImage
        {
            get { return profileImage; }
            set { profileImage = value; OnPropertyChanged(); }
        }

        private string profilePic = "user";
        public string ProfilePic
        {
            get { return profilePic; }
            set { profilePic = value; OnPropertyChanged(); }
        }

        private ImageSource profileImageOfReceiver = "user";
        public ImageSource ProfileImageOfReceiver
        {
            get { return profileImageOfReceiver; }
            set
            {
                if (value != null)
                {
                    profileImageOfReceiver = value;
                }
                else
                {
                    profileImageOfReceiver = "user";
                }
                OnPropertyChanged("ProfileImageOfReceiver");
            }
        }

        private ObservableCollection<MessageDetail> chatMessages = new ObservableCollection<MessageDetail>();
        public ObservableCollection<MessageDetail> ChatMessages
        {
            get
            {
                return chatMessages;
            }
            set
            {
                chatMessages = value;
                if (ChatMessages.Count > 0)
                {
                    chatPg.UpdateUI();
                }
                OnPropertyChanged();
            }
        }

        private MessageDetail selectedChat;
        public MessageDetail SelectedChat
        {
            get { return selectedChat; }
            set
            {
                selectedChat = value;
                if (SelectedChat != null)
                {
                    SelectedChat = null;
                }
                OnPropertyChanged();
            }
        }

        private int reqId;
        public int ReqId
        {
            get { return reqId; }
            set { reqId = value; OnPropertyChanged(); }
        }

        private string differenceMinitues;
        public string DifferenceMinitues
        {
            get { return differenceMinitues; }
            set { differenceMinitues = value; OnPropertyChanged(); }
        }

        private TimeSpan diffMinIntTimeSpan;
        public TimeSpan DiffMinIntTimeSpan
        {
            get { return diffMinIntTimeSpan; }
            set { diffMinIntTimeSpan = value; OnPropertyChanged(); }
        }

        private string currentTime;
        public string CurrentTime
        {
            get { return currentTime; }
            set { currentTime = value; OnPropertyChanged(); }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
            set { userName = value; OnPropertyChanged(); }
        }

        private int _id;
        public int id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }
        #endregion
        #region Constructor
        public ChatViewModel(int ID, int requestId, TimeSpan DiffMin, ChatPage chatPage, int isDone, string startTime, int serviceDuration)
        {
            ReqId = requestId;
            chatPg = chatPage;
            id = ID;
            CurrentTime = DateTime.Now.ToString("HH:mm");
            DiffMinIntTimeSpan = DiffMin;
            ServiceDuration = serviceDuration;
            StartTime = startTime;

            if (isDone == 0)
            {
                Device.StartTimer(TimeSpan.FromSeconds(1), OnTimerTick);
            }
            else
            {
                DiffMinIntTimeSpan = DiffMinIntTimeSpan.Add(TimeSpan.FromSeconds(1));
                DifferenceMinitues = DiffMinIntTimeSpan.ToString("g").Substring(0, 4);
            }
        }
        #endregion

        #region Commands

        private Command serviceDoneCommand;
        public Command ServiceDoneCommand
        {
            get
            {
                return serviceDoneCommand ?? new Command((obj) =>
                {
                    serviceDoneCommandExecute();
                });
            }
        }

        private Command sendMessageCommand;
        public Command SendMessageCommand
        {
            get
            {
                return sendMessageCommand ?? new Command((obj) =>
                {
                    sendMessage();
                });
            }
        }

        private Command cameraCommand;
        public Command CameraCommand
        {
            get
            {
                return cameraCommand ?? new Command((obj) =>
                {
                    cameraCommandExecute();
                });
            }
        }

        private Command noButtonCommand;
        public Command NoButtonCommand
        {
            get
            {
                return noButtonCommand ?? new Command((obj) =>
                {
                    noButtonCommandExecute();
                });
            }
        }

        private Command teaCommand;
        public Command TeaCommand
        {
            get
            {
                return teaCommand ?? new Command((obj) =>
                {
                    teaCommandExecute();
                });
            }
        }

        #endregion
        #region Methods

        //public async Task CurrentLocation()
        //{

        //    try
        //    {
        //        var locator = CrossGeolocator.Current;
        //        locator.DesiredAccuracy = 50;
        //        var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10000));
        //        var CurrentLat = position.Latitude;
        //        var CurrentLng = position.Longitude;

        //        //var CurrentLocationPin = new Pin();
        //        //CurrentLocationPin.Type = PinType.Place;
        //        //CurrentLocationPin.Label = "Your Location";
        //        //CurrentLocationPin.Position = new Position(position.Latitude, position.Longitude);
        //        //GoogleMap.Pins.Add(CurrentLocationPin);

        //        //GoogleMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromKilometers(3)));
        //    }
        //    catch (Exception ex)
        //    {
        //        ProgressDialog.HideProgress();
        //        Logger.SendErrorLog(ex);
        //    }
        //}


        bool OnTimerTick()
        {

            DiffMinIntTimeSpan = DiffMinIntTimeSpan.Add(TimeSpan.FromSeconds(1));
            DifferenceMinitues = DiffMinIntTimeSpan.ToString(); //DiffMinIntTimeSpan.Hours.ToString("00") + ":" + DiffMinIntTimeSpan.Minutes.ToString("00") + ":" + DiffMinIntTimeSpan.Seconds.ToString("00");//.ToString("g").Substring(2, 5).Remove(0, 1);
            return true;
        }

        public void CurrentTimeExecute()
        {
            CurrentTime = DateTime.Now.ToShortTimeString();//ToString("hh:mm:ss tt");
        }

        public async void cameraCommandExecute()
        {
            try
            {
                if (!CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsCameraAvailable)
                {
                    await App.Current.MainPage.DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                    return;
                }
                else
                {
                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions()
                    {
                        PhotoSize = PhotoSize.Medium,
                        CompressionQuality = 100,
                    });
                    string ImagePath;
                    if (file == null)
                        return;
                    else
                    {
                        ImagePath = file.Path;

                    }

                    Extension = Path.GetExtension(ImagePath);
                    ProfileImage = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        file.Dispose();
                        return stream;
                    });
                    var fileData = file.GetStream();
                    profileImageByte = Utilities.ToByteArray(fileData);
                    type = "image";//File.ReadAllBytes(ImagePath);
                    await sendMessage();                                                   //Convert.ToBase64String(bytes);
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        private void teaCommandExecute()
        {
            Utilities.ShowOkAlert("Comming Soon");
        }

        public async Task sendMessage()
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {

                    if (Message.IsEmpty() && profileImageByte == null)
                    {
                        Utilities.ShowOkAlert("Please write message.");
                        return;
                    }

                    ProgressDialog.ShowProgress();

                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;

                    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

                        Latitude = position.Latitude;
                        Longitude = position.Longitude;
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.NetworkAlert);
                        return;
                    }

                    sendMessageRequest req = new sendMessageRequest();
                    DateTime date = DateTime.Now;
                    req.message = Message;
                    req.user_id = GetUserPreference(Enums.KeyBox.UserId);
                    req.request_id = ReqId.ToString();
                    if (profileImageByte != null)
                        req.file = profileImageByte;
                    req.type = Type;
                    req.request_time = date.ToString("HH:mm", CultureInfo.CurrentCulture);
                    req.latitude = Latitude;
                    req.longitude = Longitude;
                    req.id = id;

                    var res = await App.userRepository.sendMessage(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            Message = string.Empty;
                            if (profileImageByte != null)
                                profileImageByte = null;
                            previousMessage(id, ReqId);
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        public async void previousMessage(int ID, int reqID)
        {
            try
            {
                Id = ID;
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    previousMessageRequest req = new previousMessageRequest();
                    req.request_id = reqID.ToString();
                    var UserID = Convert.ToInt64(GetUserPreference(Enums.KeyBox.UserId));
                    var res = await App.userRepository.previousMessage(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            if (res.message_details != null && res.message_details.Count > 0)
                            {
                                ChatMessages = res.message_details;
                                UserName = res.message_details.Where(x => x.user_id != UserID).Select(x => x.user).FirstOrDefault().user_name;
                                ProfileImageOfReceiver = res.message_details.Where(x => x.user_id != UserID).Select(x => x.user).FirstOrDefault().image;
                                foreach (var item in ChatMessages)
                                {
                                    item.IsMessage = (item.type == "text" || item.type == "link") ? true : false;
                                }
                                foreach (var item in ChatMessages)
                                {
                                    item.IsImage = (item.type == "image" || item.type == "pee" || item.type == "poo" || item.type == "nap" || item.type == "walk" || item.type == "food") ? true : false;
                                }
                                chatPg.UpdateUI();
                            }
                            //LocationUpdate();
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        private void noButtonCommandExecute()
        {
            var LastMinutes = Convert.ToDouble((DiffMinIntTimeSpan.Hours) * 60 + DiffMinIntTimeSpan.Minutes);

            if (LastMinutes < ServiceDuration)
            {
                IsVisibleSeviceDoneReasonGrid = true;
            }
            else
            {
                App.Navigation.PushAsync(new InstructionPage(DifferenceMinitues, Id, TxtReason));
            }
        }

        public void serviceDoneCommandExecute()
        {
            try
            {
                if (Helpers.IsEmpty(TxtReason))
                {
                    Utilities.ShowOkAlert("Please Enter Reason");
                    return;
                }
                else
                {
                    App.Navigation.PushAsync(new InstructionPage(DifferenceMinitues, Id, TxtReason));
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        //public async void LocationUpdate()
        //{
        //    try
        //    {
        //        var locator = CrossGeolocator.Current;
        //        locator.DesiredAccuracy = 50;

        //        if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
        //        {
        //            var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

        //            Latitude = position.Latitude;
        //            Longitude = position.Longitude;

        //            sendMessageRequest req = new sendMessageRequest();
        //            DateTime date = DateTime.Now;
        //            req.message = Message;
        //            req.user_id = GetUserPreference(Enums.KeyBox.UserId);
        //            req.request_id = ReqId.ToString();
        //            if (profileImageByte != null)
        //                req.file = profileImageByte;
        //            req.type = Type;
        //            req.request_time = date.ToString("HH:mm", CultureInfo.CurrentCulture);
        //            req.latitude = Latitude;
        //            req.longitude = Longitude;
        //            req.id = id;

        //            MessagingCenter.Send<ChatViewModel, sendMessageRequest>(this, "LocationUpdate", req);
        //        }
        //        else
        //        {
        //            ProgressDialog.HideProgress();
        //            Utilities.ShowOkAlert(Constants.NetworkAlert);
        //            return;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.SendErrorLog(ex);
        //    }
        //}

        #endregion
    }
}
