﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using PETni.Business;
using Xamarin.Forms;

namespace PETni
{
    public class HomeViewModel : BaseViewModel
    {
        #region Properties

        private string userAddress;
        public string UserAddress
        {
            get { return userAddress; }
            set { userAddress = value; OnPropertyChanged(); }
        }

        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        private ObservableCollection<IndividualServiceDetail> serviceList = new ObservableCollection<IndividualServiceDetail>();
        public ObservableCollection<IndividualServiceDetail> ServiceList
        {
            get { return serviceList; }
            set { serviceList = value; OnPropertyChanged(); }
        }

        private ObservableCollection<IndividualServiceDetail> temporaryList = new ObservableCollection<IndividualServiceDetail>();
        public ObservableCollection<IndividualServiceDetail> TemporaryList
        {
            get { return temporaryList; }
            set { temporaryList = value; OnPropertyChanged(); }
        }

        private string slotDate = DateTime.Today.ToString("yyyy-MM-dd");
        public string SlotDate
        {
            get { return slotDate; }
            set { slotDate = value; OnPropertyChanged(); }
        }

        private IndividualServiceDetail _selectedItem;
        public IndividualServiceDetail SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                if (_selectedItem != null)
                {
                    if (Helpers.IsEmpty(SelectedItem.start_time))
                    {
                        foreach (var item in ServiceList)
                        {
                            if (item.slot == SelectedItem.slot)
                            {
                                TemporaryList.Add(item);
                            }
                        }

                        var slot = EnrollmentEntityGrouping.FirstOrDefault(x => x.Contains(SelectedItem)).Key;
                        if ((slot == "All Day" || slot.Equals(GetCurrentTimeSlot())) && SelectedItem.service_date == SlotDate && SelectedItem.is_cancel == 0)
                        {
                            App.Navigation.PushAsync(new ServiceStartPage(SelectedItem.id, TemporaryList, SelectedItem.service_duration));
                        }
                        else if ((DateTime.Compare(Convert.ToDateTime(SelectedItem.service_date), DateTime.Now) > 0) && SelectedItem.is_cancel == 0)
                        {
                            App.Navigation.PushAsync(new ServiceStartPage(SelectedItem.id, TemporaryList, SelectedItem.service_duration));
                        }
                        else if (SelectedItem.is_cancel == 0)
                        {
                            App.Navigation.PushAsync(new ServiceStartPage(SelectedItem.id, TemporaryList, SelectedItem.service_duration));
                            //Utilities.ShowOkAlert("You don’t have any scheduled service for today");
                        }
                        else
                        {
                            Utilities.ShowOkAlert("This is cancelled service. Please select service which is not cancelled.");
                        }
                    }
                    else
                    {
                        //AppSession.Instance.ServiceDuration = SelectedItem.service_duration;
                        App.Navigation.PushAsync(new ChatPage(SelectedItem.id, SelectedItem.request_id, SelectedItem.start_time, SelectedItem.service_name, SelectedItem.is_done, SelectedItem.end_time, SelectedItem.service_duration), true);
                        _selectedItem = null;
                    }
                }
                OnPropertyChanged("SelectedItem");
            }
        }

        private ObservableCollection<Grouping<string, IndividualServiceDetail>> _enrollmentEntityGrouping;
        public ObservableCollection<Grouping<string, IndividualServiceDetail>> EnrollmentEntityGrouping
        {
            get
            {
                return _enrollmentEntityGrouping;
            }
            set
            {
                _enrollmentEntityGrouping = value;
                OnPropertyChanged("EnrollmentEntityGrouping");
            }
        }


        private string currentDate = DateTime.Today.ToString("MMMM dd, yyyy");
        public string CurrentDate
        {
            get { return currentDate; }
            set
            {
                currentDate = value;
                OnPropertyChanged("CurrentDate");
            }
        }
        #endregion

        #region Commands
        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsRefreshing = true;
                    getServiceList(CurrentDate);
                    IsRefreshing = false;
                });
            }
        }

        private Command leftArrowCommand;
        public Command LeftArrowCommand
        {
            get
            {
                return leftArrowCommand ?? new Command((obj) =>
                {
                    var date = Convert.ToDateTime(CurrentDate);
                    getServiceList(date.AddDays(-1).ToString("MMMM dd, yyyy"));
                });
            }
        }

        private Command rightArrowCommand;
        public Command RightArrowCommand
        {
            get
            {
                return rightArrowCommand ?? new Command((obj) =>
                {
                    var date = Convert.ToDateTime(CurrentDate);
                    getServiceList(date.AddDays(1).ToString("MMMM dd, yyyy"));
                });
            }
        }
        #endregion

        #region Construcotr

        public HomeViewModel()
        {
            TodayDate = DateTime.Today.ToString("MMMM dd, yyyy");
        }
        #endregion

        #region Methods

        public string GetCurrentTimeSlot()
        {
            try
            {
                TimeSpan morningStartTime = new TimeSpan(6, 0, 0);
                TimeSpan morningEndTime = new TimeSpan(11, 59, 59);

                TimeSpan afternoonStartTime = new TimeSpan(12, 0, 0);
                TimeSpan afternoonEndTime = new TimeSpan(17, 59, 59);

                TimeSpan eveningStartTime = new TimeSpan(18, 0, 0);
                TimeSpan eveningEndTime = new TimeSpan(20, 0, 0);

                TimeSpan now = DateTime.Now.TimeOfDay;

                if ((now > morningStartTime) && (now < morningEndTime))
                {
                    return "Morning";
                }
                if ((now > afternoonStartTime) && (now < afternoonEndTime))
                {
                    return "Afternoon";
                }
                if ((now > eveningStartTime) && (now < eveningEndTime))
                {
                    return "Evening";
                }

                return "All Day";
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
                return "";
            }
        }

        public async void getServiceList(string date)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    getServicesRequest req = new getServicesRequest();
                    req.user_id = GetUserPreference(Enums.KeyBox.UserId);
                    req.service_date = date;
                    currentDate = date;
                    TodayDate = date;

                    var res = await App.userRepository.getServices(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            //ServiceList = res.schedule.morning;
                            List<IndividualServiceDetail> tempList = new List<IndividualServiceDetail>();
                            if (res.schedule != null)
                            {
                                if (res.schedule.overnight != null && res.schedule.overnight.Count > 0)
                                {
                                    res.schedule.overnight.ForEach(x => x.schedule = "All Day");
                                    res.schedule.overnight.ForEach(x => x.FullAddress = x.customer_address.ToString());
                                    res.schedule.overnight = res.schedule.overnight.OrderBy(x => x.is_cancel).ToList();
                                    tempList.AddRange(res.schedule.overnight);
                                }

                                if (res.schedule.morning != null && res.schedule.morning.Count > 0)
                                {
                                    res.schedule.morning.ForEach(x => x.schedule = "Morning");
                                    res.schedule.morning.ForEach(x => x.FullAddress = x.customer_address.ToString());
                                    res.schedule.morning = res.schedule.morning.OrderBy(x => x.is_cancel).ToList();
                                    tempList.AddRange(res.schedule.morning);
                                }

                                if (res.schedule.afternoon != null && res.schedule.afternoon.Count > 0)
                                {
                                    res.schedule.afternoon.ForEach(x => x.schedule = "Afternoon");
                                    res.schedule.afternoon.ForEach(x => x.FullAddress = x.customer_address.ToString());
                                    res.schedule.afternoon = res.schedule.afternoon.OrderBy(x => x.is_cancel).ToList();
                                    tempList.AddRange(res.schedule.afternoon);
                                }

                                if (res.schedule.evening != null && res.schedule.evening.Count > 0)
                                {
                                    res.schedule.evening.ForEach(x => x.schedule = "Evening");
                                    res.schedule.evening.ForEach(x => x.FullAddress = x.customer_address.ToString());
                                    res.schedule.evening = res.schedule.evening.OrderBy(x => x.is_cancel).ToList();
                                    tempList.AddRange(res.schedule.evening);
                                }
                            }

                            ServiceList = new ObservableCollection<IndividualServiceDetail>(tempList);


                            var sorted = (from o in ServiceList
                                          group o by o.schedule into enrollEntityGroup
                                          select new Grouping<string, IndividualServiceDetail>(enrollEntityGroup.Key, enrollEntityGroup));
                            EnrollmentEntityGrouping = new ObservableCollection<Grouping<string, IndividualServiceDetail>>(sorted);
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
                ProgressDialog.HideProgress();
            }
            finally
            {
                //ProgressDialog.HideProgress();
            }
        }
        #endregion

    }
}
