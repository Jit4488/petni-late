﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PETni.Business;
using Plugin.Geolocator;
using Xamarin.Forms;

namespace PETni
{
    public class ServiceViewModel : BaseViewModel
    {
        #region Properties

        static int i = 0;
        int ServiceDuration;
        double Latitude = 0, Longitude = 0;

        private ObservableCollection<string> serviceList = new ObservableCollection<string>();
        public ObservableCollection<string> ServiceList
        {
            get { return serviceList; }
            set { serviceList = value; OnPropertyChanged(); }
        }

        private string _selectedItem;
        public string SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                if (_selectedItem != null)
                    _selectedItem = null;
                OnPropertyChanged("SelectedItem");
            }
        }

        private string currentTime;
        public string CurrentTime
        {
            get { return currentTime; }
            set { currentTime = value; OnPropertyChanged(); }
        }
        private ObservableCollection<ServiceDetail> currentServiceList = new ObservableCollection<ServiceDetail>();
        public ObservableCollection<ServiceDetail> CurrentServiceList
        {
            get { return currentServiceList; }
            set { currentServiceList = value; OnPropertyChanged(); }
        }
        private ServiceDetail currentService;
        public ServiceDetail CurrentService
        {
            get { return currentService; }
            set { currentService = value; OnPropertyChanged(); }
        }

        private int currnetServiceIndex = 0;
        public int CurrnetServiceIndex
        {
            get { return currnetServiceIndex; }
            set { currnetServiceIndex = value; OnPropertyChanged(); }
        }
        private string careInstructions;
        public string CareInstructions
        {
            get { return careInstructions; }
            set { careInstructions = value; OnPropertyChanged(); }
        }

        private string serviceName;
        public string ServiceName
        {
            get { return serviceName; }
            set { serviceName = value; OnPropertyChanged(); }
        }

        private ObservableCollection<IndividualServiceDetail> tempList = new ObservableCollection<IndividualServiceDetail>();
        public ObservableCollection<IndividualServiceDetail> TempList
        {
            get { return tempList; }
            set { tempList = value; OnPropertyChanged(); }
        }

        private int id = 0;
        public int Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged(); }
        }

        private string backImage = "back.png";
        public string BackImage
        {
            get { return backImage; }
            set { backImage = value; OnPropertyChanged(); }
        }

        private string nextImage = "forward.png";
        public string NextImage
        {
            get { return nextImage; }
            set { nextImage = value; OnPropertyChanged(); }
        }

        private List<PetsDetail> petDetails = new List<PetsDetail>();
        public List<PetsDetail> PetDetails
        {
            get { return petDetails; }
            set { petDetails = value; OnPropertyChanged(); }
        }

        private List<Pet> petProfileDetails = new List<Pet>();
        public List<Pet> PetProfileDetails
        {
            get { return petProfileDetails; }
            set { petProfileDetails = value; OnPropertyChanged(); }
        }

        private ObservableCollection<PetsProfile> petsProfiles = new ObservableCollection<PetsProfile>();
        public ObservableCollection<PetsProfile> PetsProfiles
        {
            get { return petsProfiles; }
            set { petsProfiles = value; OnPropertyChanged(); }
        }

        //getServicesDetailResponse
        #endregion

        #region Commands
        private Command startServiceCommand;
        public Command StartServiceCommand
        {
            get
            {
                return startServiceCommand ?? new Command((obj) =>
                {
                    StartServices();
                });
            }
        }

        private Command leftArrowCommand;
        public Command LeftArrowCommand
        {
            get
            {
                return leftArrowCommand ?? new Command((obj) =>
                {
                    //i--;
                    //var id = obj;

                    CurrnetServiceIndex = CurrnetServiceIndex - 1;
                    if (CurrnetServiceIndex >= 0)
                    {
                        if (CurrnetServiceIndex == 0)
                        {
                            BackImage = "back_gray.png";
                            NextImage = "forward.png";
                        }
                        else
                        {
                            BackImage = "back.png";
                            NextImage = "forward.png";
                        }

                        Id = TempList[CurrnetServiceIndex].id;
                        getServicesDetail(Convert.ToInt32(Id));
                    }
                    else
                    {

                        CurrnetServiceIndex = CurrnetServiceIndex + 1;
                        BackImage = "back_gray.png";
                    }
                });
            }
        }

        private Command rightArrowCommand;
        public Command RightArrowCommand
        {
            get
            {
                return rightArrowCommand ?? new Command((obj) =>
                {
                    //i++;
                    //var id = obj;
                    CurrnetServiceIndex = CurrnetServiceIndex + 1;
                    if (CurrnetServiceIndex < TempList.Count)
                    {
                        if (CurrnetServiceIndex == TempList.Count - 1)
                        {
                            NextImage = "forward_gray.png";
                            BackImage = "back.png";
                        }
                        else
                        {
                            NextImage = "forward.png";
                            BackImage = "back.png";
                        }

                        Id = TempList[CurrnetServiceIndex].id;
                        getServicesDetail(Convert.ToInt32(Id));
                    }
                    else
                    {
                        CurrnetServiceIndex = CurrnetServiceIndex - 1;
                        NextImage = "forward_gray.png";
                    }
                });
            }
        }
        private Command careInstructionCommand;
        public Command CareInstructionCommand
        {
            get
            {
                return careInstructionCommand ?? new Command((obj) =>
                {
                    CareInstructionCommandExecute();
                });
            }
        }

        private Command petProfileCommand;
        public Command PetProfileCommand
        {
            get
            {
                return petProfileCommand ?? new Command((obj) =>
                {
                    PetProfileCommandExecute();
                });
            }
        }

        #endregion

        #region Construcotr
        public ServiceViewModel(ObservableCollection<IndividualServiceDetail> serviceList, int serviceDuration)
        {
            ServiceList.Add("Don't for get care instruction below:");
            TempList = serviceList;
            ServiceDuration = serviceDuration;
            TempList = TempList.Where(x => x.is_cancel == 0).ToObservableCollection<IndividualServiceDetail>();
            TempList = TempList.Where(x => x.start_time == null).ToObservableCollection<IndividualServiceDetail>();
            Device.StartTimer(TimeSpan.FromMilliseconds(15), OnTimerTick);
            if (TempList.Count > 1)
            {
                NextImage = "forward.png";
                BackImage = "back_gray.png";
            }
            else
            {
                NextImage = "";
                BackImage = "";
            }
        }
        #endregion

        #region Methods
        public void CareInstructionCommandExecute()
        {
            try
            {
                var name = CurrentService.user_name;
                var id = CurrentService.pets_detail.FirstOrDefault().id;
                App.Navigation.PushAsync(new CareInstructionPage(id, name.ToString(), PetDetails));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }

        }

        public void PetProfileCommandExecute()
        {
            try
            {
                //Utilities.ShowOkAlert("Coming Soon...");
                App.Navigation.PushAsync(new ProfilePage(PetsProfiles));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }

        }
        bool OnTimerTick()
        {
            CurrentTime = DateTime.Now.ToString("hh:mm");
            return true;
        }
        public string GetCurrentTimeSlot()
        {
            try
            {
                TimeSpan morningStartTime = new TimeSpan(6, 0, 0);
                TimeSpan morningEndTime = new TimeSpan(11, 59, 59);

                TimeSpan afternoonStartTime = new TimeSpan(12, 0, 0);
                TimeSpan afternoonEndTime = new TimeSpan(17, 59, 59);

                TimeSpan eveningStartTime = new TimeSpan(18, 0, 0);
                TimeSpan eveningEndTime = new TimeSpan(20, 0, 0);

                TimeSpan now = DateTime.Now.TimeOfDay;

                if ((now > morningStartTime) && (now < morningEndTime))
                {
                    return "Morning";
                }
                if ((now > afternoonStartTime) && (now < afternoonEndTime))
                {
                    return "Afternoon";
                }
                if ((now > eveningStartTime) && (now < eveningEndTime))
                {
                    return "Evening";
                }

                return "All Day";
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
                return "";
            }
        }

        public async void StartServices()
        {
            try
            {

                //var cs = DateTime.ParseExact(CurrentService.service_date, "dd-M-yyyy", CultureInfo.InvariantCulture);
                //var date = DateTime.Now;
                //if ((DateTime.Compare(cs, date) > 0))
                //{
                //    Utilities.ShowOkAlert("This Service is scheduled for" + "\n" + CurrentService.service_date + "\n" + TempList.FirstOrDefault().schedule + "\n" + "You cannot start it right now");
                //    //Utilities.ShowOkAlert("This Service is scheduled for future. You cannot start it right now");
                //}
                //else 
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;

                    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

                        Latitude = position.Latitude;
                        Longitude = position.Longitude;
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.NetworkAlert);
                        return;
                    }

                    startServiceRequest req = new startServiceRequest();
                    req.user_id = GetUserPreference(Enums.KeyBox.UserId);
                    req.start_time = DateTime.Now.ToString("hh:mm:ss tt");//Convert.ToDateTime(currentTime).ToString("hh:mm:ss");
                    req.request_id = CurrentService.request_id;
                    req.id = CurrentService.id.ToString();
                    req.message = "Hi " + CurrentService.user_name + "," + "\n" + "I'm taking " + CurrentService.pets_detail[0].pet.name + " out right now";
                    req.type = "text";
                    req.latitude = Latitude;
                    req.longitude = Longitude;

                    var res = await App.userRepository.startService(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            SetUserPreference(Enums.KeyBox.UserName, CurrentService.user_name);

                            await App.Navigation.PushAsync(new ChatPage(CurrentService.id, currentService.request_id, CurrentService.start_time, ServiceName, CurrentService.is_done, CurrentService.end_time, ServiceDuration), true);

                            if (AppSession.Instance.LocationData == null)
                                AppSession.Instance.LocationData = new List<UpdateLocationRequest>();
                            AppSession.Instance.LocationData.Add(new UpdateLocationRequest { id = CurrentService.id, request_id = CurrentService.request_id, latitude = 0, longitude = 0, current_date_time = DateTime.Now.ToString("u") });

                            //if (AppSession.Instance.ServiceData == null)
                            //AppSession.Instance.ServiceData = new List<BackServiceDetails>();
                            var ServiceDataList = new List<BackServiceDetails>();
                            var ServiceStr = GetUserPreference(Enums.KeyBox.ServicesData);
                            if (ServiceStr != null)
                                ServiceDataList = Helpers.Deserialize<List<BackServiceDetails>>(ServiceStr);
                            ServiceDataList.Add(new BackServiceDetails { id = CurrentService.id, request_id = CurrentService.request_id });
                            SetUserPreference(Enums.KeyBox.ServicesData, Helpers.Serialize(ServiceDataList));

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                var timer = new System.Threading.Timer((e) =>
                                {
                                    UpdateLocation(CurrentService.id, CurrentService.request_id);
                                }, null, TimeSpan.Zero, TimeSpan.FromSeconds(30));
                            });
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        public async void getServicesDetail(int ServiceId)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    getServicesDetailRequest req = new getServicesDetailRequest();
                    req.id = ServiceId;

                    var res = await App.userRepository.getServicesDetail(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            ServiceName = res.service_detail[0].service_name;
                            CurrentServiceList = res.service_detail;
                            CurrentService = res.service_detail[0];

                            if (i >= 0 && i < res.service_detail[0].pets_detail.Count)
                            {
                                CareInstructions = res.service_detail[0].pets_detail[i].pet.care_instructions;

                                foreach (var item in res.service_detail[0].pets_detail)
                                {
                                    PetsDetail pt = new PetsDetail();
                                    pt.id = item.id;
                                    PetDetails.Add(pt);
                                }

                                if (res.service_detail[0].pets_profile.Count > 0)
                                    PetsProfiles = res.service_detail[0].pets_profile;
                                else
                                    PetsProfiles = new ObservableCollection<PetsProfile>();

                                //var PetProfileDetail = res.service_detail[0].pets_detail[0].pet;
                                //foreach (var item in res.service_detail[0].pets_detail[0].pet)
                                //{
                                //    Pet ptp = new Pet();
                                //    ptp.id = item.id;
                                //}
                            }
                            else
                            {
                                Utilities.ShowOkAlert("No more Care instructions");
                            }
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        //public async Task<bool> getLatLong()
        //{
        //    var locator = CrossGeolocator.Current;
        //    locator.DesiredAccuracy = 50;

        //    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
        //    {
        //        var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

        //        Latitude = position.Latitude;
        //        Longitude = position.Longitude;
        //        return true;
        //    }
        //    else
        //    {
        //        ProgressDialog.HideProgress();
        //        Utilities.ShowOkAlert(Constants.NetworkAlert);
        //        return false;
        //    }
        //}

        #endregion
    }
}