﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PETni.Business;
using Xamarin.Forms;

namespace PETni
{
    public class CareInstructionPageViewModel : BaseViewModel
    {
        #region Properties
        private int id = 0;
        public int Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged(); }
        }
        private List<VaccinationsReport> imageList = new List<VaccinationsReport>();
        public List<VaccinationsReport> ImageList
        {
            get { return imageList; }
            set
            {
                imageList = value;
                OnPropertyChanged();
            }
        }
        private List<VaccinationsReport> details = new List<VaccinationsReport>();
        public List<VaccinationsReport> Details
        {
            get { return details; }
            set
            {
                details = value;
                OnPropertyChanged();
            }
        }
        private string neuteredSpayed;
        public string NeuteredSpayed
        {
            get { return neuteredSpayed; }
            set { neuteredSpayed = value; OnPropertyChanged(); }
        }
        private string vaccinationsCurrent;
        public string VaccinationsCurrent
        {
            get { return vaccinationsCurrent; }
            set { vaccinationsCurrent = value; OnPropertyChanged(); }
        }
        private VaccinationsReport selectedImage;
        public VaccinationsReport SelectedImage
        {
            get { return selectedImage; }
            set
            {
                selectedImage = value;
                if (SelectedImage != null)
                {
                    SelectedImage = null;
                }
                OnPropertyChanged();
            }
        }
        private string Url;
        public string url
        {
            get { return Url; }
            set { Url = value; OnPropertyChanged(); }
        }
        private string energyLevel;
        public string EnergyLevel
        {
            get { return energyLevel; }
            set { energyLevel = value; OnPropertyChanged(); }
        }
        private string primaryVet;
        public string PrimaryVet
        {
            get { return primaryVet; }
            set { primaryVet = value; OnPropertyChanged(); }
        }
        private string emergencyVet;
        public string EmergencyVet
        {
            get { return emergencyVet; }
            set { emergencyVet = value; OnPropertyChanged(); }
        }
        private string medicalIssue;
        public string MedicalIssue
        {
            get { return medicalIssue; }
            set { medicalIssue = value; OnPropertyChanged(); }
        }
        private string behavioralIssue;
        public string BehavioralIssue
        {
            get { return behavioralIssue; }
            set { behavioralIssue = value; OnPropertyChanged(); }
        }
        private string currentService;
        public string CurrentService
        {
            get { return currentService; }
            set { currentService = value; OnPropertyChanged(); }
        }
        private string dogArrive;
        public string DogArrive
        {
            get { return dogArrive; }
            set { dogArrive = value; OnPropertyChanged(); }
        }
        private string dogHarness;
        public string DogHarness
        {
            get { return dogHarness; }
            set { dogHarness = value; OnPropertyChanged(); }
        }
        private string dogAfterWalk;
        public string DogAfterWalk
        {
            get { return dogAfterWalk; }
            set { dogAfterWalk = value; OnPropertyChanged(); }
        }
        private string walkRoutine;
        public string WalkRoutine
        {
            get { return walkRoutine; }
            set { walkRoutine = value; OnPropertyChanged(); }
        }

        private List<PetsDetail> petDetailsList = new List<PetsDetail>();
        public List<PetsDetail> PetDetailsList
        {
            get { return petDetailsList; }
            set { petDetailsList = value; OnPropertyChanged(); }
        }

        private int currnetPetDetailIndex = 0;
        public int CurrnetPetDetailIndex
        {
            get { return currnetPetDetailIndex; }
            set { currnetPetDetailIndex = value; OnPropertyChanged(); }
        }

        private string backImage;
        public string BackImage
        {
            get { return backImage; }
            set { backImage = value; OnPropertyChanged(); }
        }

        private string nextImage;
        public string NextImage
        {
            get { return nextImage; }
            set { nextImage = value; OnPropertyChanged(); }
        }
        #endregion

        #region constructer
        public CareInstructionPageViewModel(string Name, List<PetsDetail> PetDetails)
        {
            var username = Name.ToString();
            CurrentService = username;
            PetDetailsList = PetDetails;
            if (PetDetails.Count > 1)
            {
                NextImage = "forward.png";
                BackImage = "back_gray.png";
            }
            else
            {
                NextImage = "";
                BackImage = "";
            }
        }
        #endregion

        #region Command
        private Command leftArrowCommand;
        public Command LeftArrowCommand
        {
            get
            {
                return leftArrowCommand ?? new Command((obj) =>
                {
                    CurrnetPetDetailIndex = CurrnetPetDetailIndex - 1;
                    if (CurrnetPetDetailIndex >= 0)
                    {
                        if (CurrnetPetDetailIndex == 0)
                        {
                            BackImage = "back_gray.png";
                            NextImage = "forward.png";
                        }
                        else
                        {
                            BackImage = "back.png";
                            NextImage = "forward.png";
                        }

                        Id = PetDetailsList[CurrnetPetDetailIndex].id;
                        getPetCareDetails(Convert.ToInt32(Id));
                    }
                    else
                    {

                        CurrnetPetDetailIndex = CurrnetPetDetailIndex + 1;
                        BackImage = "back_gray.png";
                    }
                });
            }
        }

        private Command rightArrowCommand;
        public Command RightArrowCommand
        {
            get
            {
                return rightArrowCommand ?? new Command((obj) =>
                {
                    CurrnetPetDetailIndex = CurrnetPetDetailIndex + 1;
                    if (CurrnetPetDetailIndex < PetDetailsList.Count)
                    {
                        if (CurrnetPetDetailIndex == PetDetailsList.Count - 1)
                        {
                            NextImage = "forward_gray.png";
                            BackImage = "back.png";
                        }
                        else
                        {
                            NextImage = "forward.png";
                            BackImage = "back.png";
                        }

                        Id = PetDetailsList[CurrnetPetDetailIndex].id;
                        getPetCareDetails(Convert.ToInt32(Id));
                    }
                    else
                    {
                        CurrnetPetDetailIndex = CurrnetPetDetailIndex - 1;
                        NextImage = "forward_gray.png";
                    }
                });
            }
        }

        private Command instructionCommand;
        public Command InstructionCommand
        {
            get
            {
                return instructionCommand ?? new Command((obj) =>
                {
                    InstructionCommandExecute();
                });
            }
        }

        public void InstructionCommandExecute()
        {
            try
            {
                App.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }

        }
        #endregion

        #region Method

        public async void getPetCareDetails(int petId)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    getPetCareDetailsRequest req = new getPetCareDetailsRequest();
                    req.br_pet_id = petId;
                    var res = await App.userRepository.getPetCareDetails(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            //SetUserPreference(Enums.KeyBox.UserName,);
                            NeuteredSpayed = res.Data.neutered_spayed;
                            VaccinationsCurrent = res.Data.vaccinations_current;
                            Details = res.Data.vaccinations_reports;
                            foreach (var item in Details)
                            {
                                item.type = (item.type).ToString();
                                item.url = (item.url).ToString();
                                item.name = (item.name).ToString();
                            }
                            EnergyLevel = res.Data.energy_level;
                            PrimaryVet = res.Data.primary_vet;
                            EmergencyVet = res.Data.emergency_vet;
                            MedicalIssue = res.Data.does_your_dog_have_any_health_medical_issues;
                            BehavioralIssue = res.Data.does_your_dog_have_any_behavioral_issues;
                            DogArrive = res.Data.where_will_we_find_your_dog_when_we_arrive;
                            DogHarness = res.Data.where_will_we_find_your_dogs_leash_and_harness;
                            DogAfterWalk = res.Data.where_will_we_leave_your_dog_after_the_walk;
                            WalkRoutine = res.Data.post_walk_routine;
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }
        #endregion

    }
}
