﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using PETni.Business;
using Plugin.Geolocator;
using Xamarin.Forms;
using System.Linq;

namespace PETni
{
    public class InstructionViewModel : BaseViewModel
    {
        #region Properties
        int ID;
        string PetName, Reason;
        double Latitude = 0, Longitude = 0;

        private string differenceMinitues;
        public string DifferenceMinitues
        {
            get { return differenceMinitues; }
            set { differenceMinitues = value; OnPropertyChanged(); }
        }

        private bool leashandHarness;
        public bool LeashandHarness
        {
            get { return leashandHarness; }
            set { leashandHarness = value; OnPropertyChanged(); }
        }

        private bool charlie;
        public bool Charlie
        {
            get { return charlie; }
            set { charlie = value; OnPropertyChanged(); }
        }

        private bool feed;
        public bool Feed
        {
            get { return feed; }
            set { feed = value; OnPropertyChanged(); }
        }

        private bool postWalk;
        public bool PostWalk
        {
            get { return postWalk; }
            set { postWalk = value; OnPropertyChanged(); }
        }

        private ObservableCollection<ServiceDetail> currentServiceList = new ObservableCollection<ServiceDetail>();
        public ObservableCollection<ServiceDetail> CurrentServiceList
        {
            get { return currentServiceList; }
            set { currentServiceList = value; OnPropertyChanged(); }
        }
        private ServiceDetail currentService;
        public ServiceDetail CurrentService
        {
            get { return currentService; }
            set { currentService = value; OnPropertyChanged(); }
        }

        private string optionText1;
        public string OptionText1
        {
            get { return optionText1; }
            set { optionText1 = value; OnPropertyChanged(); }
        }

        private string optionText2;
        public string OptionText2
        {
            get { return optionText2; }
            set { optionText2 = value; OnPropertyChanged(); }
        }

        private string optionText3;
        public string OptionText3
        {
            get { return optionText3; }
            set { optionText3 = value; OnPropertyChanged(); }
        }
        #endregion

        #region Constructors
        public InstructionViewModel(string Minitues, int Id, string reason)
        {
            DifferenceMinitues = Minitues;
            ID = Id;
            Reason = reason;
        }
        #endregion

        #region Commands
        private Command noCommand;
        public Command NoCommand
        {
            get
            {
                return noCommand ?? new Command((obj) =>
                {
                    noCommandExecute();
                });
            }
        }

        private Command yesCommand;
        public Command YesCommand
        {
            get
            {
                return yesCommand ?? new Command((obj) =>
                {
                    yesCommandExecute();
                });
            }
        }

        #endregion

        #region Methods

        public async void getServicesDetail(int ServiceId)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    var res = await App.userRepository.getServicesDetail(ServiceId);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            CurrentServiceList = res.service_detail;
                            CurrentService = res.service_detail[0];
                            PetName = CurrentService.pets_detail[0].pet.name;
                            OptionText1 = "Have you left " + PetName + " in the right place?";
                            OptionText2 = "Did you follow Special Care Instruction: \"" + CurrentService.pets_detail[0].pet.care_instructions + "\" ? ";
                            OptionText3 = "Did you follow Post Walk Routine: \"" + CurrentService.pets_detail[0].pet.post_walk_routine + "\" ?";

                            //foreach(var items in res.service_detail)
                            //{
                            //    var CurrentService1 = items.pets_detail;
                            //    foreach(var details in CurrentService1)
                            //    {
                            //        var p1=details.
                            //    }

                            //}
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        private void yesCommandExecute()
        {
            if (!LeashandHarness)
            {
                Utilities.ShowOkAlert("Please select all to mark service as Complete.");
                return;
            }
            else if (!Charlie)
            {
                Utilities.ShowOkAlert("Please select all to mark service as Complete.");
                return;
            }
            else if (!Feed)
            {
                Utilities.ShowOkAlert("Please select all to mark service as Complete.");
                return;
            }
            else if (!PostWalk)
            {
                Utilities.ShowOkAlert("Please select all to mark service as Complete.");
                return;
            }
            else
            {
                endService();
            }
        }

        private void noCommandExecute()
        {
            App.Navigation.PushAsync(new HomePage());
        }

        public async void endService()
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;

                    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

                        Latitude = position.Latitude;
                        Longitude = position.Longitude;
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.NetworkAlert);
                        return;
                    }

                    endServiceRequest req = new endServiceRequest();
                    req.user_id = GetUserPreference(Enums.KeyBox.UserId);
                    req.end_time = DateTime.Now.ToString("HH:m:s"); //DifferenceMinitues;
                    req.request_id = CurrentService.request_id.ToString();
                    req.id = CurrentService.id.ToString();
                    req.message = "Hi " + CurrentService.user_name + "," + "\n" + "I'm taking " + CurrentService.pets_detail[0].pet.name + " out right now";
                    req.type = "text";
                    req.reason = Reason;
                    req.latitude = Latitude;
                    req.longitude = Longitude;

                    var res = await App.userRepository.endService(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();

                            if (AppSession.Instance.LocationData != null && AppSession.Instance.LocationData.Count > 0)
                            {
                                //foreach (var item in AppSession.Instance.LocationData)
                                //{
                                //if (item.id == CurrentService.id && item.request_id == CurrentService.request_id)
                                var list = AppSession.Instance.LocationData.Where(x => x.id == Convert.ToInt32(req.id) && x.request_id == Convert.ToInt32(req.request_id)).FirstOrDefault();
                                AppSession.Instance.LocationData.Remove(list);
                                //foreach (var item in list)
                                //{
                                //AppSession.Instance.LocationData.Remove(item);
                                //}
                                //}
                            }

                            var ServiceDataList = new List<BackServiceDetails>();
                            var ServiceStr = GetUserPreference(Enums.KeyBox.ServicesData);
                            if (ServiceStr != null)
                                ServiceDataList = Helpers.Deserialize<List<BackServiceDetails>>(ServiceStr);

                            if (ServiceDataList != null && ServiceDataList.Count > 0)
                            {
                                var list = ServiceDataList.Where(x => x.id == Convert.ToInt32(req.id) && x.request_id == Convert.ToInt32(req.request_id)).FirstOrDefault();
                                ServiceDataList.Remove(list);
                                //foreach (var item in ServiceDataList)
                                //{
                                //    if (item.id == CurrentService.id && item.request_id == CurrentService.request_id)
                                //        ServiceDataList.Remove(item);
                                //}
                            }
                            SetUserPreference(Enums.KeyBox.ServicesData, Helpers.Serialize(ServiceDataList));

                            await App.Navigation.PushAsync(new HomePage());
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }
        #endregion
    }
}
