﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PETni.Business;
using PETni;
using Xamarin.Forms;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.Geolocator;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PETni
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region InotifyPropertychangeImplement
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region StaticMethods
        public static string GetUserPreference(Enums.KeyBox key)
        {
            return App.userPreferences.GetString(key);
        }

        public static void SetUserPreference(Enums.KeyBox key, string value)
        {
            App.userPreferences.SetString(key, value);
        }
        #endregion

        #region Properties

        public static bool IsPopupOpen = false;

        private String todayDate;
        public String TodayDate
        {
            get { return todayDate; }
            set { todayDate = value; OnPropertyChanged(); }
        }

        #endregion

        #region google login
        private Command googleLoginCommand;
        public Command GoogleLoginCommand
        {
            get
            {
                return googleLoginCommand ?? new Command((obj) =>
                {
                    googleLoginCommandExecute();
                });
            }
        }

        private Command backButtonCommand;
        public Command BackButtonCommand
        {
            get
            {
                return backButtonCommand ?? new Command((obj) =>
                {
                    App.Current.MainPage = new NavigationPage(new HomePage());
                    App.Navigation = App.Current.MainPage.Navigation;

                });
            }
        }

        private Command webCommand;
        public Command WebCommand
        {
            get
            {
                return webCommand ?? new Command((obj) =>
                {
                    WebCommandExecute();
                });
            }
        }

        #endregion

        #region FacebookLogin

        private bool _isLogedIn;

        private FacebookUser _facebookUser;

        public FacebookUser FacebookUser
        {
            get { return _facebookUser; }
            set { _facebookUser = value; OnPropertyChanged("FacebookUser"); }
        }

        public bool IsLogedIn
        {
            get { return _isLogedIn; }
            set { _isLogedIn = value; OnPropertyChanged("IsLogedIn"); }
        }
        private void FacebookLogout()
        {
            DependencyService.Get<IFacebookManager>().Logout();
            IsLogedIn = false;
        }

        private Command facebookLoginCommand;
        public Command FacebookLoginCommand
        {
            get
            {
                return facebookLoginCommand ?? new Command(FacebookLogin);
            }
        }

        private void FacebookLogin()
        {
            try
            {
                DependencyService.Get<IFacebookManager>().Login(OnLoginComplete);
            }
            catch (Exception ex)
            {
                ex.SendErrorLog();
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        private void OnLoginComplete(FacebookUser facebookUser, string message)
        {
            try
            {
                if (facebookUser != null)
                {
                    FacebookUser = facebookUser;
                    IsLogedIn = true;
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await SocialLoginWithFacebook(facebookUser);
                    });
                }
                else
                {
                    Utilities.ShowOkAlert(message);
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }

        }

        public async Task SocialLoginWithFacebook(FacebookUser facebookUser)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    ProgressDialog.ShowProgress();

                    LoginRequest req = new LoginRequest();
                    req.facebook_id = facebookUser.Id;
                    //req.email = facebookUser.Email;
                    req.type = Constants.fbLoginType;
                    var DeviceToken = DependencyService.Get<IUserPreferences>().GetString(Enums.KeyBox.Token);
                    if (!string.IsNullOrEmpty(DeviceToken))
                        req.device_token = DeviceToken;
                    //req.device_token = Constants.DeviceToken;
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        req.device_type = Constants.DeviceType;
                    }
                    else
                    {
                        req.device_type = Constants.DeviceType;
                    }

                    var res = await App.userRepository.Login(req);
                    if (res != null)
                    {
                        if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                        {
                            ProgressDialog.HideProgress();
                            SetUserPreference(Enums.KeyBox.UserId, res.user_id.ToString());
                            App.Current.MainPage = new NavigationPage(new HomePage());
                            App.Navigation = App.Current.MainPage.Navigation;
                            //await App.Navigation.PushAsync(new HomePage(), true);
                        }
                        else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(res.Message);
                        }
                        else
                        {
                            ProgressDialog.HideProgress();
                            Utilities.ShowOkAlert(Constants.Error);
                        }
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.responceNull);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }
        #endregion

        #region Constructors
        public BaseViewModel()
        {
            try
            {
                //RepositoryInitiator<IUserRepository>.Instance = RepositoryInitiator<IUserRepository>.Instance;
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }

        }
        #endregion

        #region Methods

        public void WebCommandExecute()
        {
            Device.OpenUri(new Uri("https://www.petandi.com/"));
        }

        public void googleLoginCommandExecute()
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    App.Navigation.PushAsync(new LoginWithGooglePage(), true);
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        public static void RemovePreviousPage()
        {
            try
            {
                App.Navigation.RemovePage(App.Navigation.NavigationStack.ElementAt(App.Navigation.NavigationStack.Count - 2));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        public static async Task<bool> CheckPermissions(Permission permission)
        {
            var permissionStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
            bool request = false;
            if (permissionStatus == PermissionStatus.Denied)
            {
                if (Device.RuntimePlatform == Device.iOS)
                {

                    var title = $"{permission} Permission";
                    var question = $"To use this plugin the {permission} permission is required. Please go into Settings and turn on {permission} for the app.";
                    var positive = "Settings";
                    var negative = "Maybe Later";
                    var task = Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);
                    if (task == null)
                        return false;

                    var result = await task;
                    if (result)
                    {
                        CrossPermissions.Current.OpenAppSettings();
                    }

                    return false;
                }

                request = true;

            }

            if (request || permissionStatus != PermissionStatus.Granted)
            {
                var newStatus = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                if (newStatus.ContainsKey(permission) && newStatus[permission] != PermissionStatus.Granted)
                {
                    var title = $"{permission} Permission";
                    var question = $"To use the plugin the {permission} permission is required.";
                    var positive = "Settings";
                    var negative = "Maybe Later";
                    var task = Application.Current?.MainPage?.DisplayAlert(title, question, positive, negative);
                    if (task == null)
                        return false;

                    var result = await task;
                    if (result)
                    {
                        CrossPermissions.Current.OpenAppSettings();
                    }
                    return false;
                }
            }

            return true;
        }

        public static async void UpdateLocation(int id, int reqId)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {
                    //ProgressDialog.ShowProgress();

                    bool isStart = false;
                    List<UpdateLocationRequest> locationData = AppSession.Instance.LocationData;
                    if (locationData != null)
                    {
                        foreach (var item in locationData)
                        {
                            if (item.id == id && item.request_id == reqId)
                                isStart = true;
                        }
                    }
                    if (!isStart)
                        return;

                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;

                    double Latitude = 0, Longitude = 0;

                    if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
                    {
                        var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

                        Latitude = position.Latitude;
                        Longitude = position.Longitude;
                    }
                    else
                    {
                        ProgressDialog.HideProgress();
                        Utilities.ShowOkAlert(Constants.NetworkAlert);
                        return;
                    }

                    UpdateLocationRequest req = new UpdateLocationRequest();
                    req.id = id;
                    req.request_id = reqId;
                    req.latitude = Latitude;
                    req.longitude = Longitude;
                    req.current_date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    var data = JsonConvert.SerializeObject(req);

                    MessagingCenter.Send<UpdateLocationRequest, string>(req, "UpdateLocation", data);
                    //var res = await RepositoryInitiator<IUserRepository>.Instance.updateLocation(req);
                    //if (res != null)
                    //{
                    //    if (res.Error == Constants.HTTP_REQUEST_SUCCESS)
                    //    {
                    //        ProgressDialog.HideProgress();
                    //    }
                    //    else if (res.Error == Constants.HTTP_REQUEST_ERROR)
                    //    {
                    //        ProgressDialog.HideProgress();
                    //        Utilities.ShowOkAlert(res.Message);
                    //    }
                    //    else
                    //    {
                    //        ProgressDialog.HideProgress();
                    //        Utilities.ShowOkAlert(Constants.Error);
                    //    }
                    //}
                    //else
                    //{
                    //    ProgressDialog.HideProgress();
                    //    Utilities.ShowOkAlert(Constants.responceNull);
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
            finally
            {
                ProgressDialog.HideProgress();
            }
        }

        #endregion
    }
}