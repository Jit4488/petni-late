﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PETni.Business;
using Xamarin.Forms;

namespace PETni
{
    public class ProfilePageViewModel : BaseViewModel
    {
        #region Properties
        private int id = 0;
        public int Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged(); }
        }

        private int currnetPetDetailIndex = 0;
        public int CurrnetPetDetailIndex
        {
            get { return currnetPetDetailIndex; }
            set { currnetPetDetailIndex = value; OnPropertyChanged(); }
        }

        private List<PetsProfile> petDetailsList = new List<PetsProfile>();
        public List<PetsProfile> PetDetailsList
        {
            get { return petDetailsList; }
            set { petDetailsList = value; OnPropertyChanged(); }
        }

        private string petType;
        public string PetType
        {
            get { return petType; }
            set { petType = value; OnPropertyChanged(); }
        }

        private bool isBirthdate=false;
        public bool IsBirthdate
        {
            get { return isBirthdate; }
            set { isBirthdate = value; OnPropertyChanged(); }
        }

        private bool isAge=false;
        public bool IsAge
        {
            get { return isAge; }
            set { isAge = value; OnPropertyChanged(); }
        }

        private string birthdate;
        public string Birthdate
        {
            get { return birthdate; }
            set { birthdate = value; OnPropertyChanged(); }
        }

        private string age;
        public string Age
        {
            get { return age; }
            set { age = value; OnPropertyChanged(); }
        }

        private string sex;
        public string Sex
        {
            get { return sex; }
            set { sex = value; OnPropertyChanged(); }
        }

        private string dogSpayed;
        public string DogSpayed
        {
            get { return dogSpayed; }
            set { dogSpayed = value; OnPropertyChanged(); }
        }

        private string dogMicrochipped;
        public string DogMicrochipped
        {
            get { return dogMicrochipped; }
            set { dogMicrochipped = value; OnPropertyChanged(); }
        }

        private string energyLevel;
        public string EnergyLevel
        {
            get { return energyLevel; }
            set { energyLevel = value; OnPropertyChanged(); }
        }

        private string withOtherDogs;
        public string WithOtherDogs
        {
            get { return withOtherDogs; }
            set { withOtherDogs = value; OnPropertyChanged(); }
        }

        private string withOtherDogsText;
        public string WithOtherDogsText
        {
            get { return withOtherDogsText; }
            set { withOtherDogsText = value; OnPropertyChanged(); }
        }

        private string withChildren;
        public string WithChildren
        {
            get { return withChildren; }
            set { withChildren = value; OnPropertyChanged(); }
        }

        private string withChildrenText;
        public string WithChildrenText
        {
            get { return withChildrenText; }
            set { withChildrenText = value; OnPropertyChanged(); }
        }

        private string dogHousetrained;
        public string DogHousetrained
        {
            get { return dogHousetrained; }
            set { dogHousetrained = value; OnPropertyChanged(); }
        }

        private string dogHousetrainedText;
        public string DogHousetrainedText
        {
            get { return dogHousetrainedText; }
            set { dogHousetrainedText = value; OnPropertyChanged(); }
        }

        private string specialRequirements;
        public string SpecialRequirements
        {
            get { return specialRequirements; }
            set { specialRequirements = value; OnPropertyChanged(); }
        }

        private string primaryVet;
        public string PrimaryVet
        {
            get { return primaryVet; }
            set { primaryVet = value; OnPropertyChanged(); }
        }

        private string about;
        public string About
        {
            get { return about; }
            set { about = value; OnPropertyChanged(); }
        }

        private string careInstructions;
        public string CareInstructions
        {
            get { return careInstructions; }
            set { careInstructions = value; OnPropertyChanged(); }
        }

        private string breed;
        public string Breed
        {
            get { return breed; }
            set { breed = value; OnPropertyChanged(); }
        }

        private string petName;
        public string PetName
        {
            get { return petName; }
            set { petName = value; OnPropertyChanged(); }
        }

        private string weight;
        public string Weight
        {
            get { return weight; }
            set { weight = value; OnPropertyChanged(); }
        }

        private List<Images> imageList = new List<Images>();
        public List<Images> ImageList
        {
            get { return imageList; }
            set
            {
                imageList = value;
                OnPropertyChanged();
            }
        }

        private string backImage;
        public string BackImage
        {
            get { return backImage; }
            set { backImage = value; OnPropertyChanged(); }
        }

        private string nextImage;
        public string NextImage
        {
            get { return nextImage; }
            set { nextImage = value; OnPropertyChanged(); }
        }

        private ObservableCollection<PetsProfile> profileList = new ObservableCollection<PetsProfile>();
        public ObservableCollection<PetsProfile> ProfileList
        {
            get { return profileList; }
            set { profileList = value; OnPropertyChanged(); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }

        #endregion

        #region Constructors

        public ProfilePageViewModel(ObservableCollection<PetsProfile> petsProfile)
        {
            ProfileList = petsProfile;
            if (ProfileList.Count > 1)
            {
                NextImage = "forward.png";
                BackImage = "back_gray.png";
            }
            else
            {
                NextImage = "";
                BackImage = "";
            }

            BindPetDetails(petsProfile[0]);
            CurrnetPetDetailIndex = 0;
        }

        #endregion

        #region Commands
        private Command instructionCommand;
        public Command InstructionCommand
        {
            get
            {
                return instructionCommand ?? new Command((obj) =>
                {
                    InstructionCommandExecute();
                });
            }
        }

        private Command leftArrowCommand;
        public Command LeftArrowCommand
        {
            get
            {
                return leftArrowCommand ?? new Command((obj) =>
                {
                    CurrnetPetDetailIndex = CurrnetPetDetailIndex - 1;
                    if (CurrnetPetDetailIndex >= 0)
                    {
                        if (CurrnetPetDetailIndex == 0)
                        {
                            BackImage = "back_gray.png";
                            NextImage = "forward.png";
                        }
                        else
                        {
                            BackImage = "back.png";
                            NextImage = "forward.png";
                        }
                        BindPetDetails(ProfileList[CurrnetPetDetailIndex]);
                    }
                    else
                    {
                        CurrnetPetDetailIndex = CurrnetPetDetailIndex + 1;
                        BackImage = "back_gray.png";
                    }
                });
            }
        }

        private Command rightArrowCommand;
        public Command RightArrowCommand
        {
            get
            {
                return rightArrowCommand ?? new Command((obj) =>
                {
                    CurrnetPetDetailIndex = CurrnetPetDetailIndex + 1;
                    if (CurrnetPetDetailIndex < ProfileList.Count)
                    {
                        if (CurrnetPetDetailIndex == ProfileList.Count - 1)
                        {
                            NextImage = "forward_gray.png";
                            BackImage = "back.png";
                        }
                        else
                        {
                            NextImage = "forward.png";
                            BackImage = "back.png";
                        }
                        BindPetDetails(ProfileList[CurrnetPetDetailIndex]);
                    }
                    else
                    {
                        CurrnetPetDetailIndex = CurrnetPetDetailIndex - 1;
                        NextImage = "forward_gray.png";
                    }
                });
            }
        }

        #endregion

        #region Methods
        public void BindPetDetails(PetsProfile pets)
        {
            try
            {
                ProgressDialog.ShowProgress();

                PetType = pets.type;
                Breed = pets.breed;
                PetName = pets.name;
                Name = pets.name + "'s Profile";
                Weight = pets.weight;
                Birthdate = pets.birth_date;
                Age = pets.age;
                Sex = pets.sex;
                DogSpayed = pets.is_your_dog_spayed;
                DogMicrochipped = pets.is_your_dog_microchipped;
                EnergyLevel = pets.energy_level;
                WithOtherDogs = pets.does_your_dog_get_along_well_with_other_dogs;
                WithOtherDogsText = pets.does_your_dog_get_along_well_with_other_dogs_text;
                WithChildren = pets.does_your_dog_get_along_well_with_children;
                WithChildrenText = pets.does_your_dog_get_along_well_with_children_text;
                DogHousetrained = pets.is_your_dog_housetrained;
                DogHousetrainedText = pets.is_your_dog_housetrained_text;
                SpecialRequirements = pets.special_requirements;
                PrimaryVet = pets.primary_vet;
                About = pets.about_your_dog;
                CareInstructions = pets.care_instructions;
                ImageList = pets.images;

                if (pets.is_birth_date == "yes")
                {
                    IsBirthdate = true;
                    IsAge = false;
                }
                else
                {
                    IsBirthdate = false;
                    IsAge = true;
                }
                ProgressDialog.HideProgress();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        public void InstructionCommandExecute()
        {
            try
            {
                App.Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
        #endregion
    }
}
