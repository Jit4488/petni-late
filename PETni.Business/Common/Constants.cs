﻿using System;
using System.Text.RegularExpressions;

namespace PETni.Business
{
    public class Constants
    {
        public const string APP_NAME = "PETandI";

        //#if DEBUG
        //        public static string BASE_URL = "http://search.petni.com/api/";//this is for testing-development
        //#else
        //        public static string BASE_URL = "https://www.petni.com/api/";//this is for testing-development
        //#endif

        //public static string BASE_URL = "https://www.petni.com/api/";
        public static string BASE_URL = "http://search.petni.com/api/";
        //public static string BASE_URL = "https://www.petandi.com/api/";
        public const string NAMESPACE = "PETni.Business";
        public const string ServerNotResponding = "Server is not responding please retry!";

        public const int PASSWORD_LENGTH = 5;
        public const int PHONENUMBER_LENGTH = 10;
        public static readonly Regex EmailAddress = new Regex(
          @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
   + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
   + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
                [0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$");

        public const string LogoutConfirmMessage = "Are you sure you want to Logout?";
        public const string Error = "Please try again!";
        public const string responceNull = "Server not responding";
        public const string AlertTitle = APP_NAME;
        public const string OkButtonText = "OK";
        public const string NetworkMessage = "The Internet connection appears to be offline.";
        internal const string PhoneInvalid = "Phone number is invalid.";

        public const string fbLoginType = "fb";
        public const string GoogleLoginType = "gmail";
        //public const string DeviceToken = "12345678";
        //public const string DeviceType = "abc";
#if _IOS_
        public const string DeviceType = Constants.IOS;
#else
        public const string DeviceType = Constants.ANDROID;
#endif
        public const string Type = "normal";

        #region ErrorCodes
        public const int HTTP_REQUEST_SUCCESS = 200;
        public const int HTTP_REQUEST_ERROR = 400;
        #endregion

        public const string ANDROID = "android";
        public const string IOS = "ios";

        public const string DatabaseName = "PETni.sqlite";

        public const string NetworkAlert = "To continue, please turn on location tracking  on your device.";
    }
}
