﻿using System;
namespace PETni.Business
{
    public class Enums
    {
        public enum KeyBox
        {
            UserId = 1,
            UserEmail = 2,
            IsSignedUP = 3,
            EmailVerificationToken = 4,
            UserName = 5,
            Token = 6,
            LocationData = 7,
            ServicesData = 8,
            LocationSendData = 9,
            Exception = 10,
            TempData = 11,
            TempMsg = 12
        }

        public enum RegisterType
        {
            normal = 0,
            fb = 1,
            gplus = 2
        }
        public enum NotificationType
        {
            task = 1,
            chat = 2,
            Group = 3
        }
    }
}
