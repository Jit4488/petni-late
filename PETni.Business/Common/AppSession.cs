﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PETni.Business
{
    public class AppSession : BaseViewModel
    {
        private static AppSession _Instance;

        public static AppSession Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AppSession();
                }
                return _Instance;
            }
        }

        private List<UpdateLocationRequest> locationData;
        public List<UpdateLocationRequest> LocationData
        {
            get { return locationData; }
            set { locationData = value; OnPropertyChanged(); }
        }

        private List<BackServiceDetails> serviceData;
        public List<BackServiceDetails> ServiceData
        {
            get { return serviceData; }
            set { serviceData = value; OnPropertyChanged(); }
        }
    }
}
