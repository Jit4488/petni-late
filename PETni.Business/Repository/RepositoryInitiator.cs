﻿using System;
using System.Collections.Generic;

namespace PETni.Business
{
    public static class RepositoryInitiator<T>
    {
        #region Property

        // Analysis disable once StaticFieldInGenericType
        ///Set this value TRUE or FALSE if you want all calls to REAL or MOCK at one update, this will ignore value set to individually. 
        ///If you want some calls are MOCK and some are REAL then set this value to NULL
        internal static bool? IsAllReal = true;

        // Analysis disable once StaticFieldInGenericType
        internal static string TargetNameSpace = Constants.NAMESPACE;

        //Note: add string "Mock" for mock call and "Real" for real call
        //String value "Mock" and "Real" must be a folder name and namespace of the files inside it

        // Analysis disable once StaticFieldInGenericType
        internal static Dictionary<string, string> ObjectFactory = new Dictionary<string, string>() {
            {
                typeof(IUserRepository).Name,
                string.Format (TargetNameSpace +  ".UserRepository")
            }
        };

        #endregion

        #region ReadConfigValue

        internal static string ReadConfigValue(string key)
        {
            return ObjectFactory.ContainsKey(key) ? ObjectFactory[key] : string.Empty;
        }

        #endregion

        #region Instance

        public static T Instance
        {
            get
            {
                string className = ReadConfigValue(typeof(T).Name);
                var business = Activator.CreateInstance(Type.GetType(className));
                return (T)business;
            }
        }

        #endregion
    }
}
