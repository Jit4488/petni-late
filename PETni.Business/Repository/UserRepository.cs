﻿using System;
using System.Threading.Tasks;

namespace PETni.Business
{
    public class UserRepository : IUserRepository
    {
        readonly HttpRequestHelper client;
        public UserRepository()
        {
            this.client = new HttpRequestHelper();
        }

        public async Task<LoginResponse> Login(LoginRequest request)
        {
            return await this.client.CreatePostRequest<LoginResponse>("login", request);
        }

        public async Task<getServicesResponse> getServices(getServicesRequest request)
        {
            return await this.client.CreatePostRequest<getServicesResponse>("get_services", request);
        }

        public async Task<getServicesDetailResponse> getServicesDetail(getServicesDetailRequest request)
        {
            return await this.client.CreatePostRequest<getServicesDetailResponse>("get_services_detail", request);
        }

        public async Task<BaseResponce> startService(startServiceRequest request)
        {
            return await this.client.CreatePostRequest<BaseResponce>("start_service", request);
        }

        public async Task<BaseResponce> endService(endServiceRequest request)
        {
            return await this.client.CreatePostRequest<BaseResponce>("end_service", request);
        }

        public async Task<BaseResponce> sendMessage(sendMessageRequest request)
        {
            if (request.file != null)
                return await this.client.uploadFile<BaseResponce>("send_message", request);
            else
                return await this.client.CreatePostRequest<BaseResponce>("send_message", request);
        }

        public async Task<previousMessageResponse> previousMessage(previousMessageRequest request)
        {
            return await this.client.CreatePostRequest<previousMessageResponse>("previous_message", request);
        }

        public async Task<getServicesDetailResponse> getServicesDetail(int serviceId)
        {
            return await this.client.CreatePostRequest<getServicesDetailResponse>("get_services_detail?id=" + serviceId, string.Empty);
        }
        public async Task<getPetCareDetailsResponse> getPetCareDetails(getPetCareDetailsRequest request)
        {
            return await this.client.CreatePostRequest<getPetCareDetailsResponse>("pet_care_details", request);
        }
        public async Task<getPetCareDetailsResponse> getPetCareDetails(int petId)
        {
            return await this.client.CreatePostRequest<getPetCareDetailsResponse>("pet_care_details?id=" + petId, string.Empty);
        }

        public async Task<BaseResponce> updateLocation(UpdateLocationRequest request)
        {
            return await this.client.CreatePostRequest<previousMessageResponse>("update-location", request);
        }
    }
}
