﻿using System;
using System.Threading.Tasks;

namespace PETni.Business
{
    public interface IUserRepository
    {
        Task<LoginResponse> Login(LoginRequest request);

        Task<getServicesResponse> getServices(getServicesRequest request);

        Task<getPetCareDetailsResponse> getPetCareDetails(getPetCareDetailsRequest request);

        Task<getServicesDetailResponse> getServicesDetail(getServicesDetailRequest request);

        Task<BaseResponce> startService(startServiceRequest request);

        Task<BaseResponce> endService(endServiceRequest request);

        Task<BaseResponce> sendMessage(sendMessageRequest request);

        Task<previousMessageResponse> previousMessage(previousMessageRequest request);

        Task<getServicesDetailResponse> getServicesDetail(int serviceId);

        Task<getPetCareDetailsResponse> getPetCareDetails(int petId);

        Task<BaseResponce> updateLocation(UpdateLocationRequest request);
    }
}
