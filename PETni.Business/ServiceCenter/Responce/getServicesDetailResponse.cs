﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PETni.Business
{
    public class getServicesDetailResponse : BaseResponce
    {
        public ObservableCollection<ServiceDetail> service_detail { get; set; }
    }

    public partial class Images
    {
        public int id { get; set; }
        public int pet_id { get; set; }
        public object name { get; set; }
        public string image { get; set; }
        public string type { get; set; }
        public string extension { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    public class DogBreed
    {
        public int id { get; set; }
        public int pet_type_id { get; set; }
        public string name { get; set; }
        public object deleted_at { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    public class Pet
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int type { get; set; }
        public string name { get; set; }
        public string weight { get; set; }
        public int breed { get; set; }
        public string birth_date { get; set; }
        public double age { get; set; }
        public int sex { get; set; }
        public string is_your_dog_spayed { get; set; }
        public string is_your_dog_microchipped { get; set; }
        public string does_your_dog_get_along_well_with_other_dogs { get; set; }
        public string does_your_dog_get_along_well_with_other_dogs_text { get; set; }
        public string does_your_dog_get_along_well_with_cats { get; set; }
        public object does_your_dog_get_along_well_with_cats_text { get; set; }
        public string does_your_dog_get_along_well_with_children { get; set; }
        public string does_your_dog_get_along_well_with_children_text { get; set; }
        public string is_your_dog_housetrained { get; set; }
        public string is_your_dog_housetrained_text { get; set; }
        public string special_requirements { get; set; }
        public string veterinary_name_and_contact_info { get; set; }
        public int can_we_have_your_pets_vet_infomation { get; set; }
        public string about_your_dog { get; set; }
        public string care_instructions { get; set; }
        public int is_neutered { get; set; }
        public int is_vaccinations_current { get; set; }
        public string primary_vet { get; set; }
        public string emergency_vet { get; set; }
        public int has_health_issues { get; set; }
        public string health_issues { get; set; }
        public int has_behavioral_issues { get; set; }
        public string behavioral_issues { get; set; }
        public int has_allergies { get; set; }
        public string allergies { get; set; }
        public int can_treats { get; set; }
        public object treats { get; set; }
        public string where_your_dog { get; set; }
        public string leash { get; set; }
        public string where_leave_dog { get; set; }
        public string post_walk_routine { get; set; }
        public object emergency_vet_2 { get; set; }
        public int in_memory { get; set; }
        public object deleted_at { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string is_birth_date { get; set; }
        public int energy_level { get; set; }
    }

    public class PetsDetail
    {
        public int id { get; set; }
        public int b_request_id { get; set; }
        public int pet_id { get; set; }
        public object care_instruction { get; set; }
        public int is_neutered { get; set; }
        public int is_vaccinations_current { get; set; }
        public string primary_vet { get; set; }
        public string emergency_vet { get; set; }
        public int has_health_issues { get; set; }
        public string health_issues { get; set; }
        public int has_behavioral_issues { get; set; }
        public string behavioral_issues { get; set; }
        public int has_allergies { get; set; }
        public string allergies { get; set; }
        public int can_treats { get; set; }
        public object treats { get; set; }
        public string where_your_dog { get; set; }
        public string leash { get; set; }
        public string where_leave_dog { get; set; }
        public string post_walk_routine { get; set; }
        public object emergency_vet_2 { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public Pet pet { get; set; }
    }

    public class ServiceDetail
    {
        public int id { get; set; }
        public int request_id { get; set; }
        public string user_image { get; set; }
        public string user_name { get; set; }
        public string service_name { get; set; }
        public string slot { get; set; }
        public string service_date { get; set; }
        public string service_price { get; set; }
        public string addon_price { get; set; }
        public string total_price { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public int is_done { get; set; }
        public string status { get; set; }
        public int is_cancel { get; set; }
        public List<PetsDetail> pets_detail { get; set; }
        public ObservableCollection<PetsProfile> pets_profile { get; set; }
    }

    public class PetsProfile
    {
        public int id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string weight { get; set; }
        public string breed { get; set; }
        public string birth_date { get; set; }
        public string age { get; set; }
        public string sex { get; set; }
        public string is_your_dog_spayed { get; set; }
        public string is_your_dog_microchipped { get; set; }
        public string does_your_dog_get_along_well_with_other_dogs { get; set; }
        public string does_your_dog_get_along_well_with_other_dogs_text { get; set; }
        public string does_your_dog_get_along_well_with_children { get; set; }
        public string does_your_dog_get_along_well_with_children_text { get; set; }
        public string is_your_dog_housetrained { get; set; }
        public string is_your_dog_housetrained_text { get; set; }
        public string special_requirements { get; set; }
        public string about_your_dog { get; set; }
        public string care_instructions { get; set; }
        public string is_birth_date { get; set; }
        public string primary_vet { get; set; }
        public string energy_level { get; set; }
        public List<Images> images { get; set; }
        public DogBreed dog_breed { get; set; }
    }
}
