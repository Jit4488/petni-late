﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace PETni.Business
{
    public class BaseResponce : INotifyPropertyChanged
    {
        [JsonProperty("code")]
        public int Error { get; set; }

        [JsonProperty("description")]
        public string Message { get; set; }

        #region Methods
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
