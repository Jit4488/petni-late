﻿using System;
using System.Collections.Generic;

namespace PETni.Business
{
    public class getPetCareDetailsResponse : BaseResponce
    {
        public getPetCareDetailsResponseData Data { get; set; }
    }

    public class VaccinationsReport
    {
        public string type { get; set; }
        public string url { get; set; }
        public string name { get; set; }
    }

    public class getPetCareDetailsResponseData
    {
        public string neutered_spayed { get; set; }
        public string vaccinations_current { get; set; }
        public string primary_vet { get; set; }
        public string emergency_vet { get; set; }
        public string does_your_dog_have_any_health_medical_issues { get; set; }
        public string does_your_dog_have_any_health_medical_issues_text { get; set; }
        public string does_your_dog_have_any_behavioral_issues { get; set; }
        public string does_your_dog_have_any_behavioral_issues_text { get; set; }
        public string does_your_dog_have_any_food_allergies_or_sensitivities { get; set; }
        public string does_your_dog_have_any_food_allergies_or_sensitivities_text { get; set; }
        public string may_we_give_your_dog_treats { get; set; }
        public string where_will_we_find_your_dog_when_we_arrive { get; set; }
        public string where_will_we_find_your_dogs_leash_and_harness { get; set; }
        public string where_will_we_leave_your_dog_after_the_walk { get; set; }
        public string post_walk_routine { get; set; }
        public string energy_level { get; set; }
        public List<VaccinationsReport> vaccinations_reports { get; set; }
    }
}
