﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace PETni.Business
{
    public class previousMessageResponse : BaseResponce
    {
        public ObservableCollection<MessageDetail> message_details { get; set; }
    }

    public class User
    {
        public int id { get; set; }
        public string image { get; set; }
        public string user_name { get; set; }
    }

    public class MessageDetail : BaseViewModel
    {
        public int id { get; set; }
        public int b_request_id { get; set; }
        public int user_id { get; set; }
        public string message { get; set; }
        public string pet_name { get; set; }
        public string type { get; set; }
        public DateTime created_at { get; set; }
        public string updated_at { get; set; }
        public int is_read { get; set; }
        public int is_start { get; set; }
        public int is_done { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public int b_r_date_slot_id { get; set; }
        public User user { get; set; }


        [JsonIgnore]
        private bool isMessage = false;
        public bool IsMessage
        {
            get { return isMessage; }
            set { isMessage = value; OnPropertyChanged(); }
        }

        [JsonIgnore]
        private bool isImage = false;
        public bool IsImage
        {
            get { return isImage; }
            set { isImage = value; OnPropertyChanged(); }
        }

    }
}
