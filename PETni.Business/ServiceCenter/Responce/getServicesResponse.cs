﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace PETni.Business
{


    public class getServicesResponse : BaseResponce
    {
        public Schedule schedule { get; set; }
    }

    public class Schedule
    {
        public List<IndividualServiceDetail> morning { get; set; }
        public List<IndividualServiceDetail> afternoon { get; set; }
        public List<IndividualServiceDetail> evening { get; set; }
        public List<IndividualServiceDetail> overnight { get; set; }
    }

    public class CustomerAddress
    {
        public override string ToString() => $"{address}, {address_2}, {city}, {state}, {pincode}";

        public string address { get; set; }
        public string address_2 { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string pincode { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

    public class IndividualServiceDetail : BaseViewModel
    {
        private string fullAddress;
        public string FullAddress
        {
            get { return fullAddress; }
            set
            {
                fullAddress = value;
                OnPropertyChanged();
            }
        }

        public int id { get; set; }
        public int request_id { get; set; }
        public string user_image { get; set; }
        public string user_name { get; set; }
        public string service_name { get; set; }
        public string slot { get; set; }
        public int is_done { get; set; }
        public string status { get; set; }
        public string start_time { get; set; }
        public int is_cancel { get; set; }
        public string care_instructions { get; set; }
        public string end_time { get; set; }
        public int service_duration { get; set; }
        public string service_date { get; set; }
        public CustomerAddress customer_address { get; set; }

        [JsonIgnore]
        public string schedule

        { get; set; }

    }
}
