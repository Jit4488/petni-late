﻿using System;
namespace PETni.Business
{
    public class UpdateLocationRequest
    {
        public int id { get; set; }
        public int request_id { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string current_date_time { get; set; }
    }

    public class BackServiceDetails
    {
        public int id { get; set; }
        public int request_id { get; set; }
    }
}
