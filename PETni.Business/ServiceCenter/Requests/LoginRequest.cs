﻿using System;
namespace PETni.Business
{
    public class LoginRequest
    {
        public string email { get; set; }
        public string password { get; set; }
        public string type { get; set; }
        public string device_token { get; set; }
        public string device_type { get; set; }
        public string facebook_id { get; set; }
        public string google_id { get; set; }
    }
}
