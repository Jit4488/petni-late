﻿using System;
namespace PETni.Business
{
    public class endServiceRequest
    {
        public string id { get; set; }
        public string user_id { get; set; }
        public string request_id { get; set; }
        public string type { get; set; }
        public string end_time { get; set; }
        public string message { get; set; }
        public string file { get; set; }
        public string predefine_image { get; set; }
        public string reason { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
