﻿using System;
namespace PETni.Business
{
    public class startServiceRequest
    {
        public string id { get; set; }
        public string user_id { get; set; }
        public int request_id { get; set; }
        public string type { get; set; }
        public string start_time { get; set; }
        public string message { get; set; }
        public string file { get; set; }
        public string predefine_image { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
