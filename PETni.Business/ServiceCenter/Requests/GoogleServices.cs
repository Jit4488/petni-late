﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PETni.Business
{
    public class GoogleServices
    {
        public static readonly string ClientId = "209879617994-0ufc73peo67c6apku85re3s4malt400a.apps.googleusercontent.com";
        public static readonly string ClientSecret = "eFibcb6uhF7w63THg1Hzg1U1";
        public static readonly string RedirectUri = "https://www.google.co.in";

        public async Task<string> GetAccessTokenAsync(string code)
        {
            var httpClient = new HttpClient();

            httpClient.BaseAddress = new Uri("https://www.googleapis.com/oauth2/v4/token");
            var requestParameter = "code=" + code + "&client_id=" + ClientId + "&client_secret=" + ClientSecret + "&redirect_uri=" + RedirectUri + "&grant_type=authorization_code" + "&scopes=https://www.googleapis.com/auth/userinfo.email";
            StringContent str = new StringContent(requestParameter, Encoding.UTF8, "application/x-www-form-urlencoded");


            var response = await httpClient.PostAsync(new Uri("https://www.googleapis.com/oauth2/v4/token"), str);

            var json = await response.Content.ReadAsStringAsync();

            var accessToken = JsonConvert.DeserializeObject<JObject>(json).Value<string>("access_token");
            var Idtoken = JsonConvert.DeserializeObject<JObject>(json).Value<string>("id_token");

            return accessToken;
        }

        public async Task<GoogleProfileResponce> GetGoogleUserProfileAsync(string accessToken)
        {

            var requestUrl = "https://www.googleapis.com/plus/v1/people/me"
                             + "?access_token=" + accessToken;

            var httpClient = new HttpClient();

            var userJson = await httpClient.GetStringAsync(requestUrl);

            var googleProfile = JsonConvert.DeserializeObject<GoogleProfileResponce>(userJson);

            return googleProfile;
        }
    }
}
