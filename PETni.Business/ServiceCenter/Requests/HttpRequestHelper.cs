﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PETni.Business
{
    public class HttpRequestHelper
    {
        protected readonly string ServiceEndpoint;

        public HttpRequestHelper()
        {
            ServiceEndpoint = ServiceHelper.ServiceUrl;
        }

        public async Task<T> CreateGetWeatherResponse<T>(string ServiceAPIResourcePath)
        {
            try
            {
                var httpBaseClient = new HttpBaseClient(ServiceAPIResourcePath);

                var responseCRServiceContent = await httpBaseClient.Get();
                return ServiceHelper.GetResponse<T>(responseCRServiceContent);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
        }


        public async Task<T> CreateGetResponse<T>(string ServiceAPIResourcePath)
        {
            try
            {
                var httpBaseClient = new HttpBaseClient(System.IO.Path.Combine(new string[] {
                    ServiceEndpoint,
                    ServiceAPIResourcePath
                }));

                var responseCRServiceContent = await httpBaseClient.Get();
                return ServiceHelper.GetResponse<T>(responseCRServiceContent);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }
        }

        public async Task<string> CreateImagePostRequest<T>(string ServiceAPIResourcePath, object payload)
        {
            var httpBaseClient = new HttpBaseClient(System.IO.Path.Combine(new string[] {
                ServiceEndpoint,
                ServiceAPIResourcePath
            }));
            var responseServiceContent = await httpBaseClient.Post(Newtonsoft.Json.JsonConvert.SerializeObject(payload));
            return Newtonsoft.Json.JsonConvert.DeserializeObject<string>(responseServiceContent);
        }

        public async Task<T> CreatePostRequest<T>(string ServiceAPIResourcePath, object payload)
        {
            var httpBaseClient = new HttpBaseClient(System.IO.Path.Combine(new string[] {
                ServiceEndpoint,
                ServiceAPIResourcePath
            }));
            if (payload.GetType().ToString() != "System.String")
            {
                payload = Newtonsoft.Json.JsonConvert.SerializeObject(payload);
            }
            var responseServiceContent = await httpBaseClient.Post(payload.ToString());
            return ServiceHelper.GetResponse<T>(responseServiceContent);
        }
        public async Task<T> CreatePutRequest<T>(string ServiceAPIResourcePath, object payload)
        {
            var httpBaseClient = new HttpBaseClient(System.IO.Path.Combine(new string[] {
                ServiceEndpoint,
                ServiceAPIResourcePath
            }));
            if (payload.GetType().ToString() != "System.String")
            {
                payload = Newtonsoft.Json.JsonConvert.SerializeObject(payload);
            }
            var responseServiceContent = await httpBaseClient.Put(payload.ToString());
            return ServiceHelper.GetResponse<T>(responseServiceContent);
        }

        public async Task<T> CreateUploadFileRequest<T>(string ServiceAPIResourcePath, object payload)
        {
            string responseCRServiceContent = "";
            try
            {
                var httpBaseClient = new HttpBaseClient(System.IO.Path.Combine(new string[] {
                ServiceEndpoint,
                ServiceAPIResourcePath
            }));

                //if (payload.GetType ().ToString () != "System.String") {
                //  payload = Newtonsoft.Json.JsonConvert.SerializeObject (payload);
                //}
                //var responseCRServiceContent = await httpBaseClient.UploadFile(Newtonsoft.Json.JsonConvert.SerializeObject(payload).ToString());
                //var responseCRServiceContent = await httpBaseClient.UploadImage(payload);
                responseCRServiceContent = await httpBaseClient.UploadFile(payload);

                //return ServiceHelper.GetResponse<T> (responseCRServiceContent);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return ServiceHelper.GetResponse<T>(responseCRServiceContent);
        }


        public async Task<T> uploadFile<T>(string ServiceAPIResourcePath, sendMessageRequest payload)
        {
            MultipartFormDataContent content = new MultipartFormDataContent();

            StringContent imagetype = new StringContent("0");

            StringContent user_Id = new StringContent(payload.user_id.ToString());
            StringContent id = new StringContent(payload.id.ToString());
            //StringContent message = new StringContent(payload.message.ToString());
            StringContent request_id = new StringContent(payload.request_id.ToString());
            StringContent type = new StringContent(payload.type.ToString());
            StringContent request_time = new StringContent(payload.request_time.ToString());
            StringContent latitude = new StringContent(payload.latitude.ToString());
            StringContent longitude = new StringContent(payload.longitude.ToString());

            content.Add(new StreamContent(new MemoryStream(payload.file)), "file", DateTime.Now.ToString() + ".jpg");
            content.Add(user_Id, "user_id");
            content.Add(id, "id");
            //content.Add(message, "message");
            content.Add(request_id, "request_id");
            content.Add(type, "type");
            content.Add(request_time, "request_time");
            content.Add(latitude, "latitude");
            content.Add(longitude, "longitude");


            var endPoint = System.IO.Path.Combine(new string[] {
                ServiceEndpoint,
                ServiceAPIResourcePath
            });

            var client = new HttpClient();
            client.MaxResponseContentBufferSize = 2560000;
            client.Timeout = TimeSpan.FromMinutes(60);
            var responseServiceContent = await client.PostAsync(endPoint, content);
            if (responseServiceContent.IsSuccessStatusCode)
            {
                var result = await responseServiceContent.Content.ReadAsStringAsync();

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                return ServiceHelper.GetResponse<T>(result);
            }
            else
                return default(T);
        }
    }
}
