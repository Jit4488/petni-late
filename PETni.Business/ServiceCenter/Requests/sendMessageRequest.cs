﻿using System;
namespace PETni.Business
{
    public class sendMessageRequest
    {
        public string user_id { get; set; }
        public string request_id { get; set; }
        public byte[] file { get; set; }
        public string message { get; set; }
        public string type { get; set; }
        public string request_time { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int id { get; set; }
    }
}
