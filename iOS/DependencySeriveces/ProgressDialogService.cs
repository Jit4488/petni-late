﻿using System;
using BigTed;
using PETni.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ProgressDialogService))]
namespace PETni.iOS
{
    public class ProgressDialogService : IProgressDialog
    {
        public void ShowToastMessage(string message)
        {
            IsShowing = true;
            UIApplication.SharedApplication.InvokeOnMainThread(() => BTProgressHUD.ShowToast(message, true, 2));
        }

        public bool IsShowing { get; set; }

        public ProgressDialogService()
        {

        }

        #region IProgressDialog implementation

        public void Show()
        {
            this.IsShowing = true;
            BTProgressHUD.Show("", -1, ProgressHUD.MaskType.Gradient);
        }

        public void Show(string message)
        {
            IsShowing = true;
            UIApplication.SharedApplication.InvokeOnMainThread(() => BTProgressHUD.Show(message, -1, ProgressHUD.MaskType.Gradient));
        }

        public void ShowProgressWithMessage(string message, int completed, int Total)
        {
            IsShowing = true;
            var progress = completed / (float)Total;
            //completed += 0.1f;
            if (progress >= 1)
            {
                BTProgressHUD.Dismiss();
            }
            else
            {
                BTProgressHUD.Show(message, progress);
            }
        }

        public void Dismiss()
        {
            IsShowing = false;
            BTProgressHUD.Dismiss();
        }

        public void CloseKeyBoard()
        {
            UIApplication.SharedApplication.KeyWindow.EndEditing(true);
        }

        #endregion
    }
}