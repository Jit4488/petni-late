﻿using System;
using CoreLocation;
using Foundation;
using PETni.iOS;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(LocationSetup))]
namespace PETni.iOS
{
    public class LocationSetup : ILocationSetup
    {
        public LocationSetup()
        {
        }

        public bool SetLocation()
        {
            if (CLLocationManager.Status == CLAuthorizationStatus.Denied)
            {
                if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                {
                    NSString settingsString = UIApplication.OpenSettingsUrlString;
                    NSUrl url = new NSUrl(settingsString);
                    UIApplication.SharedApplication.OpenUrl(url);
                }
            }
            return true;
        }
    }
}
