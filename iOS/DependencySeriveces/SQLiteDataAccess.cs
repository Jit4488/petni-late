﻿using System;
using System.IO;
using PETni.Business;
using PETni.iOS;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteDataAccess))]
namespace PETni.iOS
{
    public class SQLiteDataAccess : ISQLiteHelper
    {
        public SQLiteDataAccess()
        {
        }

        public SQLiteConnection GetConnection()
        {
            var sqliteFilename = Constants.DatabaseName;
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
            string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
            var path = Path.Combine(libraryPath, sqliteFilename);

            // This is where we copy in the prepopulated database
            Console.WriteLine(path);
            if (!File.Exists(path))
            {
                File.Create(path);
                //File.Copy(sqliteFilename, path);
            }

            //var conn = new SQLiteConnection(plat, path, false);
            var conn = new SQLiteConnection(path);

            //var conn = new SQLiteConnection(DependencyService.Get<ISQLitePlatform>(), path, false);
            // Return the database connection 
            return conn;
        }
    }
}