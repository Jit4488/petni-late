﻿using System;
using Newtonsoft.Json;
using PETni.Business;
using PETni.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(UserPreference))]
namespace PETni.iOS
{
    public class UserPreference : IUserPreferences
    {
        public UserPreference()
        {
        }

        public void SetString(Enums.KeyBox Key, string Value)
        {
            var defaults = Foundation.NSUserDefaults.StandardUserDefaults;
            if (defaults != null)
            {
                defaults.SetString(Value != null ? Value : String.Empty, Key.ToString());
                defaults.Synchronize();
                //return true;
            }
            //return false;
        }

        public string GetString(Enums.KeyBox Key)
        {
            return Foundation.NSUserDefaults.StandardUserDefaults.StringForKey(Key.ToString());
        }


        public T Deserialize<T>(string payload)
        {
            T data = default(T);
            try
            {
                data = JsonConvert.DeserializeObject<T>(payload);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return data;
        }

        public string Serialize(object payload)
        {
            string data = null;
            try
            {
                data = JsonConvert.SerializeObject(payload);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return data;
        }
    }
}
