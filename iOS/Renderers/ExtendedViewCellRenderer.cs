﻿using System;
using System.ComponentModel;
using PETni.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(ExtendedViewCellRenderer))]
[assembly: ExportRenderer(typeof(ListView), typeof(ExtendedListViewRenderer))]
namespace PETni.iOS
{
    public class ExtendedListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.ShowsVerticalScrollIndicator = false;
            }
        }
    }
    public class ExtendedViewCellRenderer : ViewCellRenderer
    {
        private UIKit.UITableViewCell _cellCore;
        private UIKit.UIColor _unselectedBackground;
        private bool _selected;

        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);
            if (cell != null)
            {
                // Disable native cell selection color style - set as *Transparent*
                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            }
            return cell;
        }
    }
}
