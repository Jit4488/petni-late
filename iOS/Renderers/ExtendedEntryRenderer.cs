﻿using System;
using Foundation;
using PETni.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Entry), typeof(ExtendedEntryRenderer))]
namespace PETni.iOS
{
	public class ExtendedEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.BorderStyle = UITextBorderStyle.None;
				this.Control.InputAccessoryView = null;
				var view = (Entry)Element;
				if (view != null)
					if (view.IsPassword)
						Control.TextContentType = new NSString("");
			}
		}
	}
}