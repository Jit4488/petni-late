﻿using System;
using PETni;
using PETni.Business;
using PETni.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(EditorPlaceHolder), typeof(EditorPlaceholderRenderer))]
namespace PETni.iOS
{
    public class EditorPlaceholderRenderer : EditorRenderer
    {
        private string Placeholder { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            try
            {
                base.OnElementChanged(e);
                var element = this.Element as EditorPlaceHolder;
                Control.ScrollEnabled = true;
                if (Control != null && element != null)
                {
                    Placeholder = element.Placeholder;
                    if (!string.IsNullOrWhiteSpace(Placeholder))
                        Control.TextColor = UIColor.Black;
                    else
                        Control.TextColor = UIColor.Gray;
                    Control.SpellCheckingType = UITextSpellCheckingType.No;
                    Control.AutocorrectionType = UITextAutocorrectionType.No;
                    if (Control.Text == "")
                    {
                        Control.Text = Placeholder;
                        Control.TextColor = UIColor.FromRGB(155, 155, 155);
                    }
                    Control.ShouldBeginEditing += (UITextView textView) =>
                    {
                        if (textView.Text == Placeholder)
                        {
                            textView.Text = "";
                            textView.TextColor = UIColor.Black;
                        }

                        return true;
                    };
                    Control.ShouldEndEditing += (UITextView textView) =>
                    {
                        if (textView.Text == "")
                        {
                            textView.Text = Placeholder;
                            textView.TextColor = UIColor.FromRGB(155, 155, 155);
                        }

                        return true;
                    };
                    if (!element.IsEnabled)
                    {
                        element.IsEnabled = true;
                        Control.Editable = false;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            try
            {
                base.OnElementPropertyChanged(sender, e);
                var element = this.Element as EditorPlaceHolder;
                if (Control != null && element != null)
                {
                    if (!Control.Text.IsEmpty() && Control.Text != Placeholder)
                    {
                        Control.TextColor = UIColor.Black;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
