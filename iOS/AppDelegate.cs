﻿using System;
using System.Collections.Generic;
using BigTed;
using FFImageLoading.Forms.Touch;
using Firebase.CloudMessaging;
using Firebase.InstanceID;
using Foundation;
using MonoTouch.Dialog;
using PETni.Business;
using UIKit;
using UserNotifications;
using Xamarin.Forms;
using System.Reactive;
using Plugin.LocalNotifications;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using static CoreFoundation.DispatchSource;
using System.Diagnostics;
using Facebook.CoreKit;

namespace PETni.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, IUNUserNotificationCenterDelegate, IMessagingDelegate
    {
        public event EventHandler<UserInfoEventArgs> NotificationReceived;
        public static LocationManager locationManager = null;
        public static int countNotification = 0;

        private static Socket socket;
        public bool IsUnsubscirbedFromMessage = true;

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Rg.Plugins.Popup.IOS.Popup.Init();
            global::Xamarin.Forms.Forms.Init();

            Settings.AppID = "2054083061546103";
            Settings.DisplayName = "PETandI";


            CachedImageRenderer.Init();
            Xamarin.FormsMaps.Init();
            Firebase.Analytics.App.Configure(new Firebase.Analytics.Options("GoogleService-Info.plist"));
            InitSocket();
            InstanceId.Notifications.ObserveTokenRefresh(TokenRefreshNotification);

            // Register your app for remote notifications.
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // iOS 10 or later
                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
                {
                    Console.WriteLine(granted);
                });

                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.Current.Delegate = this;

                // For iOS 10 data message (sent via FCM)
                Messaging.SharedInstance.RemoteMessageDelegate = this;
            }
            else
            {
                // iOS 9 or before
                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }

            UIApplication.SharedApplication.RegisterForRemoteNotifications();

            var refreshedToken = InstanceId.SharedInstance.Token;

            if (!string.IsNullOrWhiteSpace(refreshedToken))
            {
                if (UIApplication.SharedApplication.KeyWindow != null)
                {
                    ConnectToFCM(UIApplication.SharedApplication.KeyWindow.RootViewController);
                }
            }

            // We have checked to see if the device is running iOS 8, if so we are required to ask for the user's permission to receive notifications    
            //if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            //{
            //    var notificationSettingsd = UIUserNotificationSettings.GetSettingsForTypes(
            //        UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null
            //    );

            //    app.RegisterUserNotificationSettings(notificationSettingsd);
            //}
            //if (options != null)
            //{
            //    // check for a local notification
            //    if (options.ContainsKey(UIApplication.LaunchOptionsLocalNotificationKey))
            //    {
            //        var localNotification = options[UIApplication.LaunchOptionsLocalNotificationKey] as UILocalNotification;
            //        if (localNotification != null)
            //        {
            //            UIAlertController okayAlertController = UIAlertController.Create(localNotification.AlertAction, localNotification.AlertBody, UIAlertControllerStyle.Alert);
            //            okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

            //            Window.RootViewController.PresentViewController(okayAlertController, true, null);

            //            // reset our badge
            //            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            //        }
            //    }
            //}

            //NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(5), delegate { LoadNotification(); });

            //Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            //{
            //    CrossLocalNotifications.Current.Show(Constants.APP_NAME, "Location update : " + countNotification, 1);
            //    countNotification++;
            //    return true;
            //});

            //var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
            //UIApplication.SharedApplication.RegisterUserNotificationSettings(notificationSettings);

            //LoadNotification();

            //UIApplication.SharedApplication.BeginBackgroundTask(() => { LoadNotification(); });

            this.NotificationReceived += Handle_NotificationReceived;
            LoadApplication(new App());

            locationManager = new LocationManager();
            locationManager.StartLocationUpdates();

            UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(UIApplication.BackgroundFetchIntervalMinimum);

            return base.FinishedLaunching(app, options);
        }

        public override void PerformFetch(UIApplication application, Action<UIBackgroundFetchResult> completionHandler)
        {
            LoadNotification();
            completionHandler(UIBackgroundFetchResult.NewData);
        }

        public void LoadNotification()
        {
            try
            {
                //var notification = new UILocalNotification
                //{
                //    AlertTitle = "PETni",
                //    AlertBody = "Update Location",
                //    SoundName = UILocalNotification.DefaultSoundName,
                //    FireDate = NSDate.FromTimeIntervalSinceNow(5),
                //    //RepeatCalendar = NSCalendar.CurrentCalendar,
                //    //RepeatInterval = NSCalendarUnit.Second,
                //    ApplicationIconBadgeNumber = 1
                //    //UserInfo = NSDictionary.FromObjectAndKey(NSObject.FromObject(0), NSObject.FromObject(UIApplication.LaunchOptionsLocalNotificationKey))
                //};
                //UIApplication.SharedApplication.ScheduleLocalNotification(notification);

                //CrossLocalNotifications.Current.Show(Constants.APP_NAME, "Location update", 1, DateTime.Now.AddSeconds(10));
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            // show an alert
            UIAlertController okayAlertController = UIAlertController.Create(notification.AlertAction, notification.AlertBody, UIAlertControllerStyle.Alert);
            okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

            Window.RootViewController.PresentViewController(okayAlertController, true, null);

            // reset our badge
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
        }

        public override void DidEnterBackground(UIApplication uiApplication)
        {
            MessagingCenter.Subscribe<UpdateLocationRequest, string>(this, "UpdateLocation", (sender, arg) =>
            {
                var data = JsonConvert.DeserializeObject<UpdateLocationRequest>(arg);
                var storedate = BaseViewModel.GetUserPreference(Enums.KeyBox.LocationSendData);
                if (storedate == null)
                {
                    socket.Emit("location", arg);
                    IsUnsubscirbedFromMessage = false;
                    BaseViewModel.SetUserPreference(Enums.KeyBox.LocationSendData, data.current_date_time);
                }
                else if (storedate != data.current_date_time)
                {
                    socket.Emit("location", arg);
                    IsUnsubscirbedFromMessage = false;
                    BaseViewModel.SetUserPreference(Enums.KeyBox.LocationSendData, data.current_date_time);
                }
            });
            //UserPreference userPreference = new UserPreference();
            //userPreference.SetString(Enums.KeyBox.LocationData, "DidEnterBackground");
            //NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(10), delegate { MyMethod(); });
            base.DidEnterBackground(uiApplication);
        }

        public override void WillTerminate(UIApplication uiApplication)
        {
            try
            {
                //UserPreference userPreference = new UserPreference();
                //userPreference.SetString(Enums.KeyBox.LocationData, "WillTerminate");
                //NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(10), delegate { MyMethod(); });
                base.WillTerminate(uiApplication);
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }

        #region FirebaseProperty
        public class UserInfoEventArgs : EventArgs
        {
            public NSDictionary UserInfo { get; set; }
        }
        #endregion

        #region FirebaseNotificationMethods

        [Export("messaging:didRefreshRegistrationToken:")]
        public void DidRefreshRegistrationToken(Messaging messaging, string message)
        {

        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            InstanceId.SharedInstance.SetApnsToken(deviceToken, ApnsTokenType.Unknown);
            //Auth.DefaultInstance.SetApnsToken(deviceToken, AuthApnsTokenType.Sandbox); // Production if you are ready to release your app, otherwise, use Sandbox.
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            if (NotificationReceived == null)
                return;

            var e = new UserInfoEventArgs { UserInfo = userInfo };
            NotificationReceived(this, e);
        }

        void TokenRefreshNotification(object sender, NSNotificationEventArgs e)
        {
            var refreshedToken = InstanceId.SharedInstance.Token;
            DependencyService.Get<IUserPreferences>().SetString(Enums.KeyBox.Token, InstanceId.SharedInstance.Token);
            Console.WriteLine("Device Token :" + InstanceId.SharedInstance.Token);
            ConnectToFCM(UIApplication.SharedApplication.KeyWindow.RootViewController);
        }

        [Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]
        public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            if (NotificationReceived == null)
                return;

            var e = new UserInfoEventArgs { UserInfo = notification.Request.Content.UserInfo };

            NotificationReceived(this, e);

            completionHandler(UNNotificationPresentationOptions.Alert | UNNotificationPresentationOptions.Sound);
        }

        // Receive data message on iOS 10 devices.
        public void ApplicationReceivedRemoteMessage(RemoteMessage remoteMessage)
        {
            Console.WriteLine(remoteMessage.AppData);
        }


        public static void ConnectToFCM(UIViewController fromViewController)
        {
            Messaging.SharedInstance.Connect(error =>
            {
                if (error == null)
                {
                    DependencyService.Get<IUserPreferences>().SetString(Enums.KeyBox.Token, InstanceId.SharedInstance.Token);
                    Console.WriteLine(InstanceId.SharedInstance.Token);
                }
                else
                {
                    Console.WriteLine("Let the user know that connection was successful");
                }
            });

        }

        #region Workaround for handling notifications in background for iOS 10

        [Export("userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler:")]
        public void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            if (NotificationReceived == null)
                return;

            var e = new UserInfoEventArgs { UserInfo = response.Notification.Request.Content.UserInfo };
            NotificationReceived(this, e);
        }

        #endregion

        void Handle_NotificationReceived(object sender, UserInfoEventArgs e)
        {
            try
            {
                var id = DependencyService.Get<IUserPreferences>().GetString(Enums.KeyBox.UserEmail);
                if (id != null && !string.IsNullOrEmpty(id))
                {
                    UIApplicationState state = UIApplication.SharedApplication.ApplicationState;
                    if (state == UIApplicationState.Active || state == UIApplicationState.Background || state == UIApplicationState.Inactive)
                    {
                        try
                        {
                            var messageBody = Constants.APP_NAME;
                            var notificationType = Constants.APP_NAME;
                            var messageTitle = Constants.APP_NAME;
                            var notificationSection = new MonoTouch.Dialog.Section();
                            if (e.UserInfo != null)
                            {
                                messageBody = e.UserInfo["data"].ToString();
                                notificationType = e.UserInfo["NotificationType"].ToString();
                                notificationSection.Caption = notificationType;

                                messageBody = PETni.Business.Constants.APP_NAME;

                            }
                            notificationSection.Add(new StringElement("Body", messageBody));
                        }
                        catch (Exception ex)
                        {
                            Logger.SendErrorLog(ex);
                        }
                    }
                }
                LoadNotification();
            }
            catch (Exception ex)
            {
                Logger.SendErrorLog(ex);
            }
        }
        #endregion

        #region Socket
        public void InitSocket()
        {
            if (socket != null)
            {
                socket.Close();
            }

            string url = "http://172.81.117.236:3000";
            socket = IO.Socket(url);
            socket.On("connect", data =>
            {
                if (data != null)
                {
                    var json = data as JObject;
                    string j = json.ToString(Newtonsoft.Json.Formatting.None);
                    Data d = JsonConvert.DeserializeObject<Data>(j);
                    //ChatMessageRestService.cmrr = d.data;
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                        //Debug.WriteLine(ex);
                        Logger.SendErrorLog(ex);
                    }
                    finally
                    {
                        //ProgressDialog.HideProgress();
                    }
                }
            });
            socket.Connect();
            socket.On(Socket.EVENT_CONNECT, (obj) =>
            {
                //SubscribeForMessages();
            });
            socket.On(Socket.EVENT_ERROR, (obj) =>
            {

            });
            socket.On(Socket.EVENT_CONNECT_ERROR, (obj) =>
            {

            });
            socket.On(Socket.EVENT_RECONNECT_ERROR, (obj) =>
            {

            });
            socket.On(Socket.EVENT_CONNECT_TIMEOUT, (obj) =>
            {

            });
            socket.On(Socket.EVENT_RECONNECT_FAILED, (obj) =>
            {

            });

            if (IsUnsubscirbedFromMessage)
            {
                SubscribeForMessages();
            }
        }

        public void SubscribeForMessages()
        {
            try
            {
                MessagingCenter.Subscribe<UpdateLocationRequest, string>(this, "UpdateLocation", (sender, arg) =>
                {
                    var data = JsonConvert.DeserializeObject<UpdateLocationRequest>(arg);
                    var storedate = BaseViewModel.GetUserPreference(Enums.KeyBox.LocationSendData);
                    if (storedate == null)
                    {
                        socket.Emit("location", arg);
                        IsUnsubscirbedFromMessage = false;
                        BaseViewModel.SetUserPreference(Enums.KeyBox.LocationSendData, data.current_date_time);
                    }
                    else if (storedate != data.current_date_time)
                    {
                        socket.Emit("location", arg);
                        IsUnsubscirbedFromMessage = false;
                        BaseViewModel.SetUserPreference(Enums.KeyBox.LocationSendData, data.current_date_time);
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                //MessagingCenter.Unsubscribe<UpdateLocationRequest, string>(this, "UpdateLocation");
            }
        }
        #endregion
    }
}
