﻿using System;
using System.Collections.Generic;
using PETni.Business;
using Plugin.Geolocator;
using System.Threading.Tasks;
using BigTed;
using UIKit;
using Foundation;
using Xamarin.Forms.Platform.iOS;
using Plugin.LocalNotifications;
using System.Reactive;
using CoreLocation;
using System.Linq;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace PETni.iOS
{
    public class LocationManager
    {
        protected CLLocationManager locMgr;
        // event for the location changing
        public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };

        public LocationManager()
        {
            this.locMgr = new CLLocationManager();

            this.locMgr.PausesLocationUpdatesAutomatically = false;

            // iOS 8 has additional permissions requirements
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                locMgr.RequestAlwaysAuthorization(); // works in background
                                                     //locMgr.RequestWhenInUseAuthorization (); // only in foreground
            }

            if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
            {
                locMgr.AllowsBackgroundLocationUpdates = true;
            }
            LocationUpdated += PrintLocation;
        }

        // create a location manager to get system location updates to the application
        public CLLocationManager LocMgr
        {
            get { return this.locMgr; }
        }


        public void StartLocationUpdates()
        {
            // We need the user's permission for our app to use the GPS in iOS. This is done either by the user accepting
            // the popover when the app is first launched, or by changing the permissions for the app in Settings
            if (CLLocationManager.LocationServicesEnabled)
            {

                LocMgr.DesiredAccuracy = 1; // sets the accuracy that we want in meters

                // Location updates are handled differently pre-iOS 6. If we want to support older versions of iOS,
                // we want to do perform this check and let our LocationManager know how to handle location updates.
                if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
                {
                    LocMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
                    {
                        // fire our custom Location Updated event
                        this.LocationUpdated(this, new LocationUpdatedEventArgs(e.Locations[e.Locations.Length - 1]));
                        UpdateLocation(e.Locations[e.Locations.Length - 1]);
                    };

                }
                else
                {
                    // this won't be called on iOS 6 (deprecated). We will get a warning here when we build.
                    LocMgr.UpdatedLocation += (object sender, CLLocationUpdatedEventArgs e) =>
                    {
                        this.LocationUpdated(this, new LocationUpdatedEventArgs(e.NewLocation));
                        UpdateLocation(e.NewLocation);
                    };
                }

                // Start our location updates
                LocMgr.StartUpdatingLocation();

                // Get some output from our manager in case of failure
                LocMgr.Failed += (object sender, NSErrorEventArgs e) =>
                {
                    Console.WriteLine(e.Error);
                };

            }
            else
            {
                //Let the user know that they need to enable LocationServices
                //Console.WriteLine("Location services not enabled, please enable this in your Settings");
                Utilities.ShowOkAlert(Constants.NetworkAlert);
                return;
            }
        }

        UserPreference userPreference = new UserPreference();
        public async void UpdateLocation(CLLocation location)
        {
            try
            {
                if (NetworkProvider.IsAvailable())
                {


                    var ServiceList = new List<BackServiceDetails>();
                    var ServiceStr = userPreference.GetString(Enums.KeyBox.ServicesData);
                    var ServiceStr1 = userPreference.GetString(Enums.KeyBox.Exception);
                    if (ServiceStr != null)
                        ServiceList = userPreference.Deserialize<List<BackServiceDetails>>(ServiceStr);

                    if (ServiceList != null && ServiceList.Count > 0)
                    {
                        List<UpdateLocationRequest> LocationList = new List<UpdateLocationRequest>();
                        var LocationStr = userPreference.GetString(Enums.KeyBox.LocationData);

                        if (LocationStr != null)
                            LocationList = userPreference.Deserialize<List<UpdateLocationRequest>>(LocationStr);

                        if (LocationList == null)
                            LocationList = new List<UpdateLocationRequest>();

                        UpdateLocationRequest req = new UpdateLocationRequest();

                        foreach (var item in ServiceList)
                        {
                            req.id = item.id;
                            req.request_id = item.request_id;
                            req.latitude = location.Coordinate.Latitude;
                            req.longitude = location.Coordinate.Longitude;
                            req.current_date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            var data = JsonConvert.SerializeObject(req);

                            MessagingCenter.Send<UpdateLocationRequest, string>(req, "UpdateLocation", data);
                            //LocationList.Add(req);
                        }

                        userPreference.SetString(Enums.KeyBox.LocationData, userPreference.Serialize(LocationList));
                    }
                }
            }
            catch (Exception ex)
            {
                userPreference.SetString(Enums.KeyBox.Exception, ex.ToString());
                Logger.SendErrorLog(ex);
            }
        }

        //This will keep going in the background and the foreground
        public void PrintLocation(object sender, LocationUpdatedEventArgs e)
        {
            CLLocation location = e.Location;

            Console.WriteLine("Altitude: " + location.Altitude + " meters");
            Console.WriteLine("Longitude: " + location.Coordinate.Longitude);
            Console.WriteLine("Latitude: " + location.Coordinate.Latitude);
            Console.WriteLine("Course: " + location.Course);
            Console.WriteLine("Speed: " + location.Speed);

        }

        ////const string NotificationKey = UIApplication.LaunchOptionsLocalNotificationKey;

        //public LocationManager()
        //{
        //    var notification = new UILocalNotification
        //    {
        //        AlertTitle = "PETni",
        //        AlertBody = "Update Location",
        //        SoundName = UILocalNotification.DefaultSoundName,
        //        FireDate = NSDate.FromTimeIntervalSinceNow(10),
        //        //RepeatCalendar = NSCalendar.CurrentCalendar,
        //        //RepeatInterval = NSCalendarUnit.Second,
        //        UserInfo = NSDictionary.FromObjectAndKey(NSObject.FromObject(0), NSObject.FromObject(UIApplication.LaunchOptionsLocalNotificationKey))
        //    };
        //    UIApplication.SharedApplication.ScheduleLocalNotification(notification);

        //    //UpdateLocation();
        //    //CrossLocalNotifications.Current.Show("PETni", "Location Update", 10);
        //}

        //public async void UpdateLocation()
        //{
        //    try
        //    {
        //        //var manager = new CLLocationManager();
        //        //manager.DesiredAccuracy = 10;
        //        //manager.ActivityType = CLActivityType.Other;
        //        //manager.AllowsBackgroundLocationUpdates = true;
        //        //manager.RequestAlwaysAuthorization();

        //        //UILocalNotification uILocal = new UILocalNotification();
        //        //BTProgressHUD.ShowToast(DateTime.Now.ToString(), true, 3000);

        //        UserPreference userPreference = new UserPreference();
        //        List<UpdateLocationRequest> LocationList = new List<UpdateLocationRequest>();
        //        var LocationStr = userPreference.GetString(Enums.KeyBox.LocationData);

        //        if (LocationStr != null)
        //            LocationList = userPreference.Deserialize<List<UpdateLocationRequest>>(LocationStr);

        //        if (LocationList == null)
        //            LocationList = new List<UpdateLocationRequest>();

        //        UpdateLocationRequest req = new UpdateLocationRequest();

        //        var locator = CrossGeolocator.Current;
        //        locator.DesiredAccuracy = 50;

        //        double Latitude = 0, Longitude = 0;

        //        if (locator.IsGeolocationAvailable && locator.IsGeolocationEnabled)
        //        {
        //            var position = await locator.GetPositionAsync(TimeSpan.FromMilliseconds(10000));

        //            Latitude = position.Latitude;
        //            Longitude = position.Longitude;
        //        }
        //        else
        //        {
        //            ProgressDialog.HideProgress();
        //            userPreference.SetString(Enums.KeyBox.LocationData, "Location not available");
        //            Utilities.ShowOkAlert("To continue, let your device turn on location using Google's location service");
        //            return;
        //        }

        //        req.id = 0;
        //        req.request_id = 1;
        //        req.latitude = Latitude;
        //        req.longitude = Longitude;
        //        req.current_date_time = DateTime.Now;
        //        LocationList.Add(req);

        //        userPreference.SetString(Enums.KeyBox.LocationData, userPreference.Serialize(LocationList));
        //        BTProgressHUD.ShowToast("Location Update", true, 2000);
        //        await Task.Delay(5000);

        //        UpdateLocation();
        //    }
        //    catch (Exception ex)
        //    {
        //        UserPreference userPreference = new UserPreference();
        //        userPreference.SetString(Enums.KeyBox.LocationData, "Error : " + ex.Message);
        //        Logger.SendErrorLog(ex);
        //    }
        //}
    }
}
